# Fidelio_hls
Fidelio_hls is an experimental high-level synthesis tool (HLS).

HLS is the process that transforms of a "high" level language (generally C) into a Register-transfer level representation.

As for now, Fidelio just start with an intermediate representation and generates VHDL code.

The goal is to experiment all the phases of HLS :
- IR design
- CDFG representations
- scheduling algorithms
- allocation and mapping algorithms
- code generation
