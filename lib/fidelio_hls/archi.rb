require_relative 'graph'
require_relative 'code'

module FidelioHLS

  class FSMD

    attr_accessor :name
    attr_accessor :datapath,:controler

    def initialize name,controler,datapath
      @name=name
      @datapath=datapath
      @controler=controler
    end

    def inputs
      datapath.inputs
    end

    def outputs
      datapath.outputs
    end
  end

  class Datapath  < GRAPH::Graph
    include Log
    attr_accessor :components, :inputs,:outputs

    def initialize name
      super(name)
      @inputs,@outputs,@components=[],[],[]
    end

    def connect p1,p2
      @nodes << p1 unless @nodes.include?(p1) #graph stuff
      @nodes << p2 unless @nodes.include?(p2) #graph stuff
      unless connected?(p1,p2)
        p1.to(p2)
        log_dbg "\t\t-datapath connects : #{p1.inspect}-->#{p2.inspect}"
      end
      @components||=[]
      @components << p1.component unless @components.include?(p1.component) or p1.component==self
      @components << p2.component unless @components.include?(p2.component) or p2.component==self
      @components.uniq!

      @inputs||=[]
      @inputs << p1 if (p1.is_a?(HW::Input) and p1.component==self and !@inputs.include?(p1))
      @inputs << p2 if (p2.is_a?(HW::Input) and p2.component==self and !@inputs.include?(p2))

      @outputs||=[]
      @outputs << p1 if (p1.is_a?(HW::Output) and p1.component==self and !@outputs.include?(p1))
      @outputs << p2 if (p2.is_a?(HW::Output) and p2.component==self and !@outputs.include?(p2))
    end

    def deconnect p1,p2
      unconnect(p1,p2)
      suppress_unconnected_ports()
    end

    def delete_component component
      res=@components.delete(component)
      log_dbg "\t\t-datapath deleted component #{res.inspect}"
    end

    def suppress_unconnected_ports
      #puts "suppress unconnected port"
      cands=[]
      cands << @nodes.select{|port| port.pred_edges.empty? and port.succ_edges.empty?}
      cands.flatten!
      cands.each{|port| @nodes.delete(port)}
    end

    def to_dot
      code=Code.new
      code << "digraph #{self.name}{"
      code.indent=2
      each_node do |port|
        component=port.component
        comp=(component==self) ? port : component
        label=comp.name
        code << "#{comp.object_id} [label=\"#{label}\"] # #{port.inspect}"
      end
      each_edge do |wire|
        port_source=wire.source
        port_sink  =wire.sink
        source=((comp_1=port_source.component)==self) ? port_source : comp_1
        sink  =((comp_2=port_sink.component)==self) ? port_sink : comp_2
        label="#{port_source.name}->#{port_sink.name}"
        code << "#{source.object_id} -> #{sink.object_id} [label=\"#{label}\"]"
      end
      code.indent=0
      code << "}"
      code
    end

    def save_as filename
      code=self.to_dot
      code.save_as filename
    end

    def signals
      self.edges.map{|e| e.infos[:signal]}
    end
  end

  class State  < GRAPH::Node
    attr_accessor :name,:controls,:branching,:done
    def initialize name
      @name=name
      @controls=[]
      @branching=nil
      super()
    end

    def add_control control_h
      @controls << control_h
      @controls.uniq!
    end

    def add_branch ctrlIte
      @branching=ctrlIte
    end

    def add_done!
      @done=true
    end

    def inspect
      name
    end

    def next_states
      self.succs
    end
  end

  Transfer = Struct.new(:mux_prod_cmd,:mux_cons_cmd)
  CtrlAlu  = Struct.new(:cstep,:op)
  CtrlIte  = Struct.new(:status_reg,:true_state,:false_state)
  Signal   = Struct.new(:name,:type)

  class Controler < GRAPH::Graph

    attr_accessor :states_h
    attr_accessor :states
    def add_bb_state bb,state
      @states_h||={}
      @states_h[bb]||=[]
      @states_h[bb] << state
      @states||=[]
      @states << state unless @states.include?(state)
      @nodes << state #I am a graph
    end

    #============== visualization stuff =======================
    def to_dot
      code=Code.new
      code << "digraph #{self.name}{"
      code.indent=2
      each_node do |state|
        label=state.inspect
        code << "#{state.object_id} [label=\"#{label}\"]"
      end
      each_edge do |edge|
        label=edge.infos.values.first.to_s if edge.infos.any?
        label.gsub!(/\"/,"") if label
        code << "#{edge.source.object_id} -> #{edge.sink.object_id}[label=\"#{label}\"]"
      end
      each_node do |state|
        code << command_rect(state)
      end
      code.indent=0
      code << "}"
      code
    end

    # add a rectangle node that contains the datapath controls
    def command_rect state
      code =Code.new
      label=""
      state.controls.each do |control_h|
        control_h.each do |hw,cmd_value|
          label+="#{hw.partial_inspect} => #{cmd_value}\n"
        end
      end
      if state.done
        label+="done!"
      end
      node="commands_#{state.object_id}"
      code << "#{node} [style=dashed,color=grey,shape=rectangle,label=\"#{label}\"]"
      code << "#{state.object_id} -> #{node}[color=grey,style=dashed,arrowhead=none]"
      return code
    end

    def save_as filename
      code=self.to_dot
      code.save_as filename
    end

  end

end
