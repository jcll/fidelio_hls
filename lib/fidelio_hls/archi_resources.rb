require_relative 'graph'

module FidelioHLS

  module HW

    class ResourcePool

      def initialize
        @resources=[]
      end

      def allocate resource
        @resources << resource
      end

      def get_alus
        @resources.select{|r| r.is_a? Alu}
      end

      def get_alus_for op,alus_arg=nil
        alus=alus_arg || get_alus()
        candidates_alus=alus.select{|alu| alu.operations.include? op}
      end

      def get_regs_for var
        regs=@resources.select{|r| r.is_a? Reg}
        regs.select{|r| r.reusable}
        #regs.select{|reg| reg.type==var.type}
      end

      def get_ports_for io
        case io
        when Read
          ports=@resources.select{|r| r.is_a? Input}
        when Write
          ports=@resources.select{|r| r.is_a? Output}
        end
        #ports.select{|port| port.type==io.type}
      end

      def get_mem_access_for node
        mem_name=node.ast.name
      end

    end

    class Resource
      attr_accessor :name
      attr_accessor :id
      attr_accessor :occupied_at
      attr_accessor :muxes
      attr_accessor :ports
      attr_accessor :implementation_of
      attr_accessor :infos
      def initialize name=nil
        @occupied_at={}
        @implementation_of=[]
        @muxes=[]
        @ports=[]
        @name=name || @id.to_s
        @infos={}
      end

      def add_input type
        name="I#{inputs.size}"
        raise "no type given for add_input(type)" unless type
        @ports << input=HW::Input.new(self,name,type)
        return input
      end

      def inputs
        @ports.select{|p| p.is_a? HW::Input}
      end

      def outputs
        @ports.select{|p| p.is_a? HW::Output}
      end

      def input
        inputs.first
      end

      def output
        outputs.first
      end

      def implements node
        @implementation_of << node
        @implementation_of.uniq!
      end

      def to_at node,position,infos={}
        e=GRAPH::Edge.new(self,node,infos)
        @succ_edges << e
        raise "OVER WRITTING ERROR : port #{position} already used" if node.pred_edges[position]
        node.pred_edges[position]=e
      end

      def get_connexion_position node
        node.pred_edges.index{|edge| edge.source==self}
      end

      def get_mux idx=0
        unless @muxes[idx]
          @muxes[idx]=Mux.new(:int8)
        end
        return @muxes[idx]
      end

      def occupied_during? interval
        ! free_during?(interval)
      end

      def free_during? interval
        occupancy(interval).all? nil
      end

      def occupancy interval
        p interval.map{|cstep| @occupied_at[cstep]}
      end

      def get_input_fed_by port
        for input in inputs
          for edge in input.pred_edges
            if edge.source==port
              return edge.sink
            end
          end
        end
        return nil
      end

      def get_port_named str
        ports.find{|port| port.name==str}
      end

      def inspect
        name
      end

      def partial_inspect
        name
      end

    end

    class Mux < Resource
      @@id=0
      attr_accessor :type
      def initialize type=:int8
        super()
        @type=type
        @id=@@id
        @@id+=1
        @name="MUX#{@id}"
        @ports << Output.new(self,"F",type)
      end

      def partial_inspect
        "MUX_#{@id}"
      end

      def inspect
        "MUX_#{@id}{#{inputs.size}}[#{@type}]"
      end

      def input_index port
        inputs.index(port)
      end

      def size
        inputs.size
      end

    end

    class Port < GRAPH::Node
      @@id=0
      attr_accessor :component
      attr_accessor :name,:type
      attr_accessor :occupied_at
      def initialize component,name,type
        super()
        @component=component
        @name=name
        @type=type
        @occupied_at={}
        @implementation_of=[]
      end

      def signals
        succ_edges.map{|edge| edge.infos[:signal]}
      end

      def fanin
        @pred_edges.size
      end

      def nb_incoming_wires
        @pred_edges.size
      end

      def implements node
        @implementation_of << node
      end

      def inspect
        "#{component.name}.#{name}{#{@id}}[#{@type}]"
      end

      def partial_inspect
        "#{name}"
      end

      def set_front_mux type
        unless mux=get_front_mux()
          mux=HW::Mux.new(type)
          mux.output.to self
        end
        return mux
      end

      def get_front_mux
        if edge=pred_edges[0]
          port_mux=edge.source
          if (mux=port_mux.component).is_a? Mux
            return mux
          end
          return nil
        else
          return nil
        end
      end

      def get_sink_components
        succ_edges.map{|edge| edge.sink.component}
      end
    end

    class Input < Port
      # def inspect
      #   "#{self.component.name}.#{name}/#{@id}[#{@width}]"
      # end
    end

    class Output < Port
      # def inspect
      #   "#{self.component.name}.#{name}/#{@id}[#{@width}]"
      # end
    end

    class Reg < Resource
      @@id=0
      attr_accessor :type,:reusable
      def initialize type,reusable=true
        super()
        @type=type
        @resusable=reusable
        @id=@@id
        @@id+=1
        @ports << Input.new(self,"d",type)
        @ports << Output.new(self,"q",type)
        @name="R#{@id}"
      end

      def inspect
        "R#{@id}[#{@type}]"
      end

      def partial_inspect
        name
      end
    end

    class Ram < Resource
      @@id=0
      attr_accessor :addr_type,:data_type
      def initialize addr_type,data_type
        super()
        @addr_type=addr_type
        @data_type=data_type
        @id=@@id
        @@id+=1
        @ports << Input.new(self,"address"  ,addr_type)
        @ports << Input.new(self,"datain"  ,data_type)
        @ports << Output.new(self,"dataout"  ,data_type)
        @name="RAM#{@id}"
      end

      def inspect
        "RAM#{@id}[#{@addr_type},#{@data_type}]"
      end

      def partial_inspect
        name
      end
    end

    class Wire < Resource
      @@id=0
      def initialize type
        super()
        @type=type
        @id=@@id
        @@id+=1
        @name="W#{@id}"
        @ports << Input.new(self,"I",type)
        @ports << Output.new(self,"O",type)
      end

      def inspect
        name
      end
    end

    class Cst < Resource
      attr_accessor :value
      def initialize type,value
        super()
        @type=type
        @value=value
        @ports << Output.new(self,"OUT",:int8)
        @name="CST_#{@value.sexp}"
      end

      def type
          @value.type #type is as the ast level
      end

      def inspect
        "#{@value.val}/Cst[#{@value.sexp}#{@width}]"
      end

    end

    class Alu < Resource
      @@id=0
      attr_accessor :operations
      attr_accessor :type
      def initialize op,type=:int8
        super()
        @type=type
        @id=@@id
        @operations=[op]
        @@id+=1
        @ports << HW::Input.new(self,"I0",@type)
        @ports << HW::Input.new(self,"I1",@type)
        @ports << HW::Output.new(self,"F",@type)
        @name="ALU#{@id}"
      end

      def add_operation op
        @operations << op unless @operations.include?(op)
      end

      def inspect
        "#{name} #{@operations}"
      end

      def partial_inspect
        name
      end
    end

    class StatusReg < Resource
      @@id=-1
      attr_accessor :cond
      attr_accessor :type
      def initialize cond
        super()
        @id=(@@id+=1)
        @cond=cond
        @ports << HW::Input.new(self,"D",:int8)
        @ports << HW::Output.new(self,"Q",:int8)
        @type=:bit
        @name="STATUS#{@id}"
      end

      def inspect
        name
      end

      def partial_inspect
        name
      end
    end
  end # module Hardware
end
