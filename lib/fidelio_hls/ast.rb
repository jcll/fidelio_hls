require_relative 'graph'
require_relative 'cdfg_drawer'

module FidelioHLS

  class Ast
    attr_accessor :node
    def accept(visitor, arg=nil)
       name = self.class.name.split(/::/).last
       visitor.send("visit#{name}".to_sym, self ,arg) # Metaprograming !
    end
  end

  class CDFG < Ast
    attr_accessor :name
    attr_accessor :type_defs
    attr_accessor :var_defs
    attr_accessor :bbs
    attr_accessor :datapath

    def initialize
      @type_defs=[]
      @var_defs=[]
      @bbs=[]
      @datapath=nil
    end

    def each &block
      @bbs.each &block
    end

    def draw filename=nil
      CDFGDrawer.new.draw(self,filename)
    end
  end

  class TypeDef < Ast
    attr_accessor :name,:definition
  end

  class VarDef < Ast
    attr_accessor :name,:type
  end

  #===== type =========
  class Type < Ast
  end

  class UnknownType < Type
    attr_accessor :name
  end

  class BasicType < Type
    attr_accessor :name
  end

  class ArrayType < Type
    attr_accessor :size,:element_type
  end

  #==============================
  class BasicBlock < Ast
    attr_accessor :name,:stmts
    attr_accessor :dfg
    def initialize
      @stmts=[]
      @dfg=nil
    end

  end

  #===== statement ====
  class Stmt < Ast
  end

  class Io < Stmt
    attr_accessor :var,:port
  end

  class Read < Io
  end

  class Write < Io
  end

  class Stop < Stmt
  end

  class Assign < Stmt
    attr_accessor :lhs,:rhs
    def sexp
      "(assign #{lhs.sexp} #{rhs.sexp})"
    end
  end

  class Branch < Stmt
    attr_accessor :succs
    def initialize
      @succs=[]
    end

    def << bb
      @succs << bb
    end
  end

  class Goto < Branch
    def initialize bb
      super()
      @succs << bb
    end

    def succ
      @succs.first
    end
  end

  class Ite < Branch
    attr_accessor :cond

    def trueBranch
      @trueBranch=@succs.first
    end

    def falseBranch
      @falseBranch=@succs.last
    end

    def trueBranch=bb
      @succs[0]= bb
    end

    def falseBranch=bb
      @succs[1]=bb
    end

  end

  class MWrite < Stmt
    attr_accessor :mem,:addr,:expr
  end

  class Puts < Stmt
    attr_accessor :str
  end

  #==== expressions=====
  class Expr < Ast
    attr_accessor :type
  end

  class MRead < Expr
    attr_accessor :mem,:addr
  end

  class Binary < Expr
    attr_accessor :lhs,:op,:rhs
  end

  class Unary < Expr
    attr_accessor :op,:expr
  end

  class Const < Expr
    attr_accessor :val

    def initialize val,type=:int8
      @val=val
      @type=type
    end

  end

  class Var < Expr
    attr_accessor :name

    def initialize name,type=:int8
      @name=name
      @type=type
    end

    def var #trick
      self
    end

  end

  #=========== CDFG stuff =========
  class Port < Expr
    attr_accessor :name
    def initialize name
      @name=name
    end
  end

  class Mem < Expr
    attr_accessor :name
    def initialize name
      @name=name
    end
  end

end
