require_relative 'graph'

class Symbol
  def sexp
    self
  end
end

module FidelioHLS

  class CDFG < Ast
    def sexp
      code=Code.new
      code << "(cdfg #{name}"
      code.indent=2
      type_defs.each{|td| code << td.sexp}
      code.newline if type_defs.any?
      var_defs.each{|vd| code << vd.sexp}
      code.newline if var_defs.any?
      bbs.each{|bb| code << bb.sexp}
      code.indent=0
      code << ")"
      code.finalize
    end
  end

  class TypeDef < Ast
    def sexp
      "(type #{name} #{definition.sexp})"
    end
  end

  class VarDef < Ast
    def sexp
      "(var #{name} #{type.sexp})"
    end
  end
  #=============================================
  class UnknownType < Type
    def sexp
      name
    end
  end

  class BasicType < Type
    def sexp
      name
    end
  end

  class ArrayType < Type
    def sexp
      "(array #{size} #{element_type.sexp})"
    end
  end
  #=============================================
  class BasicBlock < Ast
    def sexp
      code=Code.new
      code << "(basicblock #{name}"
      code.indent=2
      stmts.each{|stmt| code << stmt.sexp}
      code.indent=0
      code << ")"
      code
    end
  end

  #===== statement ====
  class Stmt < Ast
  end

  class Read < Io
    attr_accessor :var,:port
    def sexp
      "(read #{var.sexp} #{port.sexp})"
    end
  end

  class Write < Io
    def sexp
      "(write #{var.sexp} #{port.sexp})"
    end
  end

  class MWrite < Stmt
    def sexp
      "(mwrite #{mem.sexp} #{addr.sexp} #{expr.sexp})"
    end
  end

  class Stop < Stmt
    def sexp
      "(stop)"
    end
  end

  class Puts < Stmt
    def sexp
      "(puts \"#{str}\")"
    end
  end

  class Assign < Stmt
    def sexp
      "(assign #{lhs.sexp} #{rhs.sexp})"
    end
  end


  class Goto < Branch
    def sexp
      bb=((f=@succs.first).is_a? Symbol) ? f : f.name
      "(goto #{bb})"
    end
  end

  class Ite < Branch
    def sexp
      lhs=((s0=@succs[0]).is_a? Symbol) ? s0 : s0.name
      rhs=((s1=@succs[1]).is_a? Symbol) ? s1 : s1.name
      "(ite #{cond.sexp} #{lhs} #{rhs})"
    end
  end

  #==== expressions=====
  class Expr < Ast
  end

  class MRead < Expr
    def sexp
      "(mread #{mem.sexp} #{addr.sexp})"
    end
  end

  class Binary < Expr
    def sexp
      l=lhs.sexp
      r=rhs.sexp
      "(#{op} #{l} #{r})"
    end
  end

  class Unary < Expr
    def sexp
      e=expr.sexp
      "(#{op} #{e})"
    end
  end

  class Const < Expr
    def sexp
      val.to_s
    end
  end

  class Var < Expr
    def sexp
      "#{name}"
    end
  end

  #=========== CDFG stuff =========
  class Port < Expr
    def sexp
      name
    end
  end

  class Mem < Expr
    def sexp
      name
    end
  end

end
