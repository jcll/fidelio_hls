require_relative 'graph'

class Symbol
  def sexp_hw
    self
  end
end

module FidelioHLS

  class CDFG < Ast
    def sexp_hw
      code=Code.new
      code << "(cdfg #{name}"
      code.indent=2
      type_defs.each{|td| code << td.sexp_hw}
      code.newline if type_defs.any?
      var_defs.each{|vd| code << vd.sexp_hw}
      code.newline if var_defs.any?
      bbs.each{|bb| code << bb.sexp_hw}
      code.indent=0
      code << ")"
      code
    end
  end

  class TypeDef < Ast
    def sexp_hw
      "(type #{name} #{definition.sexp_hw})"
    end
  end

  class VarDef < Ast
    def sexp_hw
      "(var #{name} #{type.sexp_hw})"
    end
  end
  #=============================================
  class UnknownType < Type
    def sexp_hw
      name
    end
  end

  class BasicType < Type
    def sexp_hw
      name
    end
  end

  class ArrayType < Type
    def sexp_hw
      "(array #{size} #{element_type.sexp_hw})"
    end
  end
  #=============================================
  class BasicBlock < Ast
    def sexp_hw
      code=Code.new
      code.newline
      code << "(basicblock #{name}"
      code.indent=2
      stmts.each{|stmt| code << stmt.sexp_hw}
      code.indent=0
      code << ")"
      code
    end
  end

  #===== statement ====
  class Stmt < Ast
  end

  class Read < Io
    attr_accessor :var,:port
    def sexp_hw
      "(read #{var.sexp_hw} #{port.sexp_hw})"
    end
  end

  class Write < Io
    def sexp_hw
      "(write #{var.sexp_hw} #{port.sexp_hw})"
    end
  end

  class MWrite < Stmt
    def sexp_hw
      "(mwrite #{mem.sexp_hw} #{addr.sexp_hw} #{expr.sexp_hw})"
    end
  end

  class Stop < Stmt
    def sexp_hw
      "(stop)"
    end
  end

  class Puts < Stmt
    def sexp_hw
      "(puts \"#{str}\")"
    end
  end

  class Assign < Stmt
    def sexp_hw
      "(assign #{lhs.sexp_hw} #{rhs.sexp_hw})"
    end
  end

  class Branch < Stmt
    attr_accessor :succs
    def initialize
      @succs=[]
    end

    def << bb
      @succs << bb
    end
  end

  class Goto < Branch
    def sexp_hw
      bb=((f=@succs.first).is_a? Symbol) ? f : f.name
      "(goto #{bb})"
    end
  end

  class Ite < Branch
    def sexp_hw
      lhs=((s0=@succs[0]).is_a? Symbol) ? s0 : s0.name
      rhs=((s1=@succs[1]).is_a? Symbol) ? s1 : s1.name
      "(ite #{cond.sexp_hw} #{lhs} #{rhs})"
    end
  end

  #==== expressions=====
  class Expr < Ast
  end

  class MRead < Expr
    def sexp_hw
      cstep=node.cstep
      ram=node.mapping.name
      "({cs=#{cstep}} #{ram} #{addr.sexp_hw})"
    end
  end

  class Binary < Expr
    def sexp_hw
      l=lhs.sexp_hw
      r=rhs.sexp_hw
      alu=node.mapping.name
      cstep=node.cstep
      "({cs=#{cstep}} #{alu}.#{op} #{l} #{r})"
    end
  end

  class Unary < Expr
    def sexp_hw
      e=expr.sexp_hw
      alu=node.mapping.name
      "(#{alu}.#{op} #{e})"
    end
  end

  class Const < Expr
    def sexp_hw
      self.node.mapping.name
    end
  end

  class Var < Expr
    def sexp_hw
      name
      node.mapping.name
    end
  end

  #=========== CDFG stuff =========
  class Port < Expr
    def sexp_hw
      name
    end
  end

  class Mem < Expr
    def sexp_hw
      name
    end
  end

end
