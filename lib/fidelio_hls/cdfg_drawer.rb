require_relative 'code'

module FidelioHLS

  class CDFGDrawer

    def draw cdfg,filename=nil
      puts "[+] drawing cdfg"
      @cdfg=cdfg
      @branch_edges=[]
      code=Code.new
      code << "digraph #{cdfg.name} {"
      code.indent=2
      cdfg.each{|bb| code << cluster(bb)}
      @branch_edges.each{|k| code << k}
      code.indent=0
      code << "}"
      filename||="#{cdfg.name}_cdfg.dot"
      code.save_as filename
      puts " |--> generated as '#{filename}'"
    end

    def cluster bb
      code=Code.new
      code << "subgraph cluster#{bb.name} {"
      code.indent=2
      code << "label=\"#{bb.name}\""
      code << dfg_code=get_code_for(bb)
      @branch_edges << branch_edges(bb,dfg_code)
      code.indent=0
      code << "}"
      code
    end

    def get_code_for bb
      final=Code.new
      final << bb.dfg.to_dot.lines[1..-2]
      final
    end

    def branch_edges bb,dfg_code
      if ite=dfg_code.lines.grep(/ite|goto/).first
        code=Code.new
        lhead="cluster#{bb.name}"
        br_id=ite.match(/(\d+)/)[1]
        case branch=bb.stmts.last
        when Ite
          nodes=branch.succs.map{|succ| "_start_#{succ.name}"}
          code << "#{br_id} -> #{nodes.shift} [label=\"True\",lhead=#{lhead}]"
          code << "#{br_id} -> #{nodes.shift} [label=\"False\",lhead=#{lhead}]"
        when Goto
          nodes=branch.succs.map{|succ| "_start_#{succ.name}"}
          code << "#{br_id} -> #{nodes.shift}[lhead=#{lhead}];"
        end
        return code
      else
        return ""
      end
    end

  end
end
