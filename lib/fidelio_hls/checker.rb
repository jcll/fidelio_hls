require_relative 'visitor'
require_relative 'printer'

module FidelioHLS

  class Checker < Visitor

    include Log

    def check cdfg
      log"[+] checking cdfg '#{cdfg.name}'"
      visitCDFG(cdfg)
      build_symtable(cdfg)
      ensure_goto(cdfg)
      link_bbs(cdfg)
    end

    private

    def ensure_goto cdfg
      cdfg.bbs.each_with_index do |bb,idx|
        case bb.stmts.last
        when Stop,Ite,Goto
        else
          puts "basicblock #{bb.name} has no ending branch."
          next_bb=cdfg.bbs[idx+1]
          cdfg.bbs[idx].stmts << Goto.new(next_bb.name)
        end
      end
    end


    def link_bbs cdfg
      @bb_names_h=cdfg.bbs.collect{|bb| [bb.name,bb]}.to_h
      @visited=[]
      bb0=cdfg.bbs.first
      link_rec(bb0)
    end

    def link_rec bb
      log " |--[+] '#{bb.name}'"
      @visited << bb
      case branch=bb.stmts.last
      when Branch
        puts "#{branch.sexp}"
        branch.succs.each_with_index do |succ,idx|
          succ=@bb_names_h[succ] #symbol->BB object
          branch.succs[idx]=succ # connec BB object
          link_rec(succ) unless @visited.include?(succ)
        end
      end
    end

    def build_symtable cdfg
      log_dbg " |--[+] building symbol table"
      @symtable={}
      cdfg.var_defs.each do |var_def|
        @symtable[var_def.name]=var_def
      end
    end


    def visitVar var,args=nil
      #puts "visiting var #{var.inspect}"
    end

  end
end
