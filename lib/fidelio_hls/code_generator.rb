require_relative 'code'
require_relative 'vhdl'
require_relative 'vhdl_printer'

module FidelioHLS

  class CodeGenerator
    def generate fsmd
      puts "[+] code generation"
      generate_fsmd fsmd
      generate_controler fsmd
      generate_datapath fsmd
    end

    def generate_fsmd fsmd
      name="#{fsmd.name}_fsmd"
      puts "  |-->[+] generating #{name}.vhd"
      model        = VHDL::Model.new(name)
      model.entity = VHDL::Entity.new(name,[],[])
      model.entity.ports << VHDL::Input.new("start","std_logic")
      model.entity.ports << VHDL::Output.new("done" ,"std_logic")

      fsmd.inputs.each {|input | model.entity.ports << VHDL::Input.new(input.name,input.type)}
      fsmd.outputs.each{|output| model.entity.ports << VHDL::Output.new(output.name,output.type)}

      model.archi  = VHDL::Architecture.new("rtl",model.entity,[],[])
      model.archi.body  << VHDL::Instance.new("controler_l",fsmd.name,"#{fsmd.name}_controler","RTL",[],
                              port_map=[
                                "reset_n" => "reset_n",
                                "clk"     => "clk",
                                "start"   => "start",
                                "done"    => "done",
                                "status"  => "status",
                                "controls"=> "controls",
                              ]
                          )

      model.archi.body  << VHDL::Instance.new("datapath_l",fsmd.name,"#{fsmd.name}_controler","RTL",[],
                              port_map=[
                                "reset_n" => "reset_n",
                                "clk"     => "clk",
                                "status"  => "status",
                                "controls"=> "controls",
                              ]
                          )
      fsmd.inputs.each{|p| port_map << {p.name => p.name}}
      fsmd.outputs.each{|p| port_map << {p.name => p.name}}

      VHDL::Printer.new().print(model)
    end

    def generate_controler fsmd
      name="#{fsmd.name}_controler.vhd"
      puts "  |-->[+] generating #{name}"
      code=Code.new
      code.save_as name
    end

    def generate_datapath fsmd
      name="#{fsmd.name}_datapath.vhd"
      puts "  |-->[+] generating #{name}"
      code=Code.new
      code.save_as name
    end

    def ieee_header
      code=Code.new
      code << "library ieee;"
      code << "use ieee.std_logic_1164.all;"
      code << "use ieee.numeric_std.all;"
    end
  end
end
