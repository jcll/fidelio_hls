require 'sxp'

require_relative 'version'
require_relative 'log'
require_relative 'ast'
require_relative 'parser'
require_relative 'printer'
require_relative 'checker'
require_relative 'transformer'
require_relative 'dfg_builder'
require_relative 'cdfg_drawer'
require_relative 'scheduler'
require_relative 'mapper'
require_relative 'fsmd_creator'
require_relative 'reporter'
require_relative 'code_generator'
require_relative 'vhdl_generator'
require_relative 'sequential_interpreter'

module FidelioHLS

  class Compiler

    include Log
    attr_accessor :options

    def initialize
      $log=File.open("fidelio.log",'w')
      log_dbg "fidelio start at : #{Time.now.strftime("%a %d %B %Y %H:%M")}"
    end

    def header
      log "-"*80
      log "CDFG compiler - version #{VERSION}"
      log "author : jean-christophe.le_lann@ensta-bretagne.fr"
      log "-"*80
    end

    def compile sexpfile
      header

      cdfg=Parser.new.parse(sexpfile)
      Printer.new.print(cdfg)
      Checker.new.check(cdfg)

      # cdfg_new=Transformer.new.transform(cdfg)
      # Printer.new.print(cdfg_new)
      # abort

      DFGBuilder.new.build(cdfg)
      cdfg.draw
      if options[:interpret]
        interpret(cdfg)
      else
        hls(cdfg)
      end
      close
    end

    def hls cdfg
      Scheduler.new.schedule(cdfg)
      Mapper.new.map(cdfg)
      fsmd=FsmdCreator.new.create_from(cdfg)
      Reporter.new.report(cdfg)
      VHDLGenerator.new.generate(fsmd)
    end

    def interpret cdfg
      interpreter=SequentialInterpreter.new
      interpreter.run(cdfg)
    end

    def close
      log "[+] closing : log is #{$log.inspect}"
    end

  end
end
