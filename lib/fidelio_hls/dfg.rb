require_relative 'graph'

module FidelioHLS

  class DFG < GRAPH::Graph
    def select &block
      @nodes.select &block
    end
  end

  class DFGNode < GRAPH::Node
    attr_accessor :mapping
    attr_accessor :cstep

    def initialize infos={}
      super(infos)
      ast.node=self
      @mapping=nil
    end

    def ast
      @infos[:ast]
    end

    def map_on resource
      @mapping=resource
      resource.implements self
    end

    def inspect
      "Node[#{ast.sexp}]"
    end

  end

  class DataNode < DFGNode
    # some nodes simply embeds a variable.
    # this method fixes a BUG during code generation.
    def name
      @mapping.name
    end

    def pred
      self.preds.first
    end

    def succ
      self.succs.first
    end
  end

  class IoNode < DFGNode
  end

  class ComputeNode < DFGNode
    def op
      ast=@infos[:ast]
      ret=(ast.respond_to? :op) ? ast.op : nil
      ret
    end
  end

  class ConstNode < DFGNode
  end

  class ControlNode < DFGNode
  end

  class MemNode < DFGNode
  end

end
