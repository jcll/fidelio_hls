require_relative 'dfg'
require_relative 'dfg_drawer'
require 'colorize'

module FidelioHLS

  class DFGBuilder
    include Log

    def build cdfg
      log "[+] building dfgs for '#{cdfg.name}'"
      @temp_var=-1
      cdfg.bbs.each{|bb| build_dfg(bb)}
    end

    def get_tmp
      @temp_var+=1
      "$#{@temp_var}"
    end

    def build_dfg bb
      log " |--[+] #{bb.name}"

      @current_bb=bb
      @producers={}
      @dfg=DFG.new(bb.name)
      make_dfg bb
      simplify @dfg
      log "     |-->[+] completed"
      @dfg.save_as "#{bb.name}.dot"
      bb.dfg=@dfg
    end

    def handle_precedence node,stmt_idx
      mem_name=node.ast.mem.name
      #puts "access #{mem_name} - stmt #{stmt_idx}"
      last_access=@last_access[mem_name]
      if last_access
        puts "adding precedence edge #{last_access.inspect} ---> #{node.inspect}"
        @dfg.connect(last_access,node,precedence:true)
      end
      @last_access[mem_name]=node
    end

    def make_dfg bb
      @last_access={} #mem_name => last node accessing mem
      bb.stmts.each_with_index do |stmt,stmt_idx|
        assign=ite=read=mwrite=write=goto=stop=stmt
        case stmt
        when Assign
          lhs_node=DataNode.new(ast:assign.lhs)
          rhs_node=build_node(assign.rhs,stmt_idx)
          if (v=assign.lhs).is_a? Var
            @producers[v.name]=lhs_node
            #label={var:v}
          end
          #@dfg.connect(rhs_node,lhs_node,label)
          @dfg.connect(rhs_node,lhs_node)
        when Ite
          cond_node=build_node(ite.cond,stmt_idx)
          @dfg << node=ControlNode.new(ast:ite)
          @dfg.connect(cond_node,node)
        when Read
          @dfg << dnode=IoNode.new(ast:read)
          @dfg << vnode=DataNode.new(ast:read.var)
          @dfg.connect dnode,vnode
          @producers[read.var.name]=vnode
        when MWrite
          addr_node=build_node(mwrite.addr,stmt_idx)
          expr_node=build_node(mwrite.expr,stmt_idx)
          @dfg << node=MemNode.new(ast:mwrite)
          @dfg.connect addr_node,node,addr:true
          @dfg.connect expr_node,node
          handle_precedence(node,stmt_idx)
        when Write
          @dfg << dnode=IoNode.new(ast:write)
          vnode=build_node(write.var,stmt_idx)
          @dfg.connect(vnode,dnode)
        when Goto
          @dfg << node=ControlNode.new(ast:goto)
        when Stop
          @dfg << node=ControlNode.new(ast:stop)
        end
      end
    end

    def build_node expr,stmt_idx
      var=unary=binary=const=port=mread= expr
      case expr
      when Const
        unless dnode=@producers[const.val]
          dnode=@producers[const.val]=ConstNode.new(ast:const)
          @dfg << dnode
        else
          expr.node=dnode #traceability purpose
        end
      when Var
        unless dnode=@producers[var.name]
          dnode=@producers[var.name]=DataNode.new(ast:var)
        else
          var.node=dnode #traceability purpose
        end
        @producers[var.name]=dnode
      when MRead
        addr_node=build_node(mread.addr,stmt_idx)
        @dfg << mnode=MemNode.new(ast:mread)
        @dfg.connect addr_node,mnode,addr:true
        @dfg << dnode=DataNode.new(ast:Var.new(get_tmp()))
        @dfg.connect mnode,dnode
        handle_precedence(mnode,stmt_idx)
      when Binary
        l=build_node(expr.lhs,stmt_idx)
        r=build_node(expr.rhs,stmt_idx)
        @dfg << cnode=ComputeNode.new(ast:binary)
        @dfg.connect(l,cnode)
        @dfg.connect(r,cnode)
        @dfg << dnode=DataNode.new(ast:Var.new(get_tmp()))
        @dfg.connect(cnode,dnode)
      when Unary
        u=build_node(unary.expr,stmt_idx)
        @dfg << cnode=ComputeNode.new(ast:unary)
        @dfg.connect(u,cnode)
        @dfg << dnode=DataNode.new(ast:Var.new(get_tmp()))
        @dfg.connect(cnode,dnode)
      else
        raise "ERROR : unknow expression '#{expr}' [class=#{expr.class}]"
      end
      return dnode
    end

    def simplify dfg
      deletables=[]
      dfg.each_edge do |edge|
        source,sink=edge.source,edge.sink
        if source.is_a?(DataNode) and sink.is_a?(DataNode)
          deletables << source
          pred=source.preds.first
          dfg.connect pred,sink if pred
        end
      end
      deletables.each{|node| dfg.delete(node)}
      log "     |-->[+] simplifying : #{deletables.size} nodes removed"
    end

  end
end
