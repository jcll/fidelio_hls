require_relative 'graph'
require_relative 'ast_sexp'

module FidelioHLS

  class DFG < GRAPH::Graph

    #overrriding graph.to_dot
    def to_dot
      dot_code=Code.new
      dot_code << "digraph #{name} {"
      dot_code.indent=2
      # dummy start node for BB anchoring
      start="_start_#{self.name}"
      dot_code << "#{start} [shape=point]"
      @id={}
      @usage={}
      nodes.each_with_index do |node,idx|
        shape="oval"
        id=node.object_id
        @var_usage={}
        case ast=node.ast
        when Binary
          label=ast.op
        when Var
          label=ast.name.to_s
          shape="rect"
          @usage[label]||=0
          @usage[label]+=1
        when Read
          label="?#{ast.port.name}"
        when MRead
          #shape="rect"
          label="#{ast.mem.name}[]"
        when MWrite
          #shape="rect"
          label="#{ast.mem.name}[]="
        when Write
          dot_code << "{ rank = sink; #{id}; }"
          label="!#{ast.port.name}"
        when Const
          shape="rect"
          label="#{ast.val}"
        when Unary
          label="#{ast.op}"
        when Ite
          shape="diamond"
          dot_code << "{ rank = sink; #{id}; }"
          label="ite"
        when Goto
          shape="diamond"
          dot_code << "{ rank = sink; #{id}; }"
          label="goto"
        when Stop
          shape="diamond"
          dot_code << "{ rank = sink; #{id}; }"
          label="stop"
        else
          raise "ERROR : from dfg_draw :#{ast.class}"
        end

        xlabel_elems=[]
        if hw=node.mapping
          xlabel_elems << hw.partial_inspect
        end

        if cstep=node.cstep
          xlabel_elems << "CS"+cstep.to_s
        end

        xlabel="xlabel=\"#{xlabel_elems.join("-")}\""

        dot_code << "#{id} [#{xlabel}label=\"#{label}\",shape=#{shape}]"

        case ast
        when Read,Var,Const
          dot_code << "#{start} -> #{id} [style=invis]"
        end

      end

      each_edge do |edge|
        labels=[]
        source=@id[edge.source]||edge.source.object_id
        sink  =@id[edge.sink]  ||edge.sink.object_id
        transfer_h=edge.infos[:transfer]
        if transfer_h
          mux,val=transfer_h.first
          transfer="#{mux.partial_inspect} => #{val}"
          labels << "label=\"#{transfer}\""
        end
        if edge.infos[:addr]
          labels << "label=\"@\""
        end
        if edge.infos[:precedence]
          labels << "style=dashed, color=grey"
        end
        label="[#{labels.join(',')}]"
        dot_code << "#{source} -> #{sink}#{label};"
      end
      dot_code.indent=0
      dot_code << "}"
      return dot_code
    end

  end
end
