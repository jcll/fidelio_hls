require "colorize"

module FidelioHLS

  class FsmdCreator
    include Log

    def create_from cdfg
      log "[+] creating FSMD for #{cdfg.name}"
      datapath =create_datapath(cdfg)
      controler=create_controler(cdfg)
      cdfg.draw "#{cdfg.name}_cdfg_transfer.dot"
      return FSMD.new(cdfg.name,controler,datapath)
    end

    def create_datapath cdfg
      log " |-->[+] creating datapath"
      datapath=cdfg.datapath # previously created, needed during Mapping
      add_muxes(cdfg)
      connect_resources(cdfg,datapath)
      simplify(cdfg,datapath)
      name_signals(datapath)
      datapath.save_as "#{datapath.name}_datapath.dot"
      return datapath
    end

    def is_var_node? node
      node.ast.is_a? Var
    end

    def get_input resource,idx
      resource.is_a?(HW::Port) ? resource : resource.inputs[idx]
    end

    def get_output resource,idx=0
      resource.is_a?(HW::Port) ? resource : resource.outputs[idx]
    end

    def set_front_mux datapath,port,type
      unless mux=port.get_front_mux()
        log_dbg "#{port.inspect} set_front_mux -> #{type}"
        raise "set_front_mux : NO type specified !" unless type
        mux=HW::Mux.new(type)
        datapath.connect mux.output,port
      end
      return mux
    end

    def add_muxes cdfg
      log "      |-->[+] adding muxes"
      datapath=cdfg.datapath
      cdfg.each do |bb|
        resources=bb.dfg.nodes.collect{|node| node.mapping}
        alus=resources.select{|res| res.is_a? HW::Alu}
        alus.each do |alu|
          alu.inputs.each do |input|
            set_front_mux(datapath,input,input.type)
          end
        end
        var_nodes=bb.dfg.select{|n| n.is_a? DataNode}.reject{|n| n.mapping.is_a? HW::Wire}
        var_nodes.each do |var_node|
          var=var_node.ast
          reg=var_node.mapping
          mux=set_front_mux(datapath,reg.inputs.first,var.type)
          mux_input=mux.get_input_fed_by(reg.output) || mux.add_input(var.type)
          datapath.connect reg.output,mux_input #back to register mux selection (enable)
        end
        outputs=resources.select{|res| res.is_a? HW::Output}
        outputs.each do |output|
          set_front_mux(datapath,output,output.type)
        end
        status_regs=resources.select{|res| res.is_a? HW::StatusReg}
        status_regs.each do |reg|
          reg.inputs.each do |input|
            set_front_mux(datapath,input,input.type)
          end
        end
        mem_accesses=resources.select{|res| res.is_a? HW::Ram}
        mem_accesses.each do |mem_access|
          mem_access.inputs.each do |input|
            set_front_mux(datapath,input,input.type)
          end
        end
      end
    end

    def connect_resources cdfg,datapath
      log "      |-->[+] connecting resources"
      cdfg.each do |bb|
        log_dbg "           |--> basic block #{bb.name}"
        bb.dfg.each_edge do |edge|
          next if edge.infos[:precedence]
          log_dbg "-"*80
          source,sink=edge.source,edge.sink
          var_node=(source.is_a? DataNode) ? source : sink
          hsource=source.mapping
          hsink=sink.mapping
          log "-> #{source.ast.sexp} mapped on #{hsource.inspect} -> #{sink.ast.sexp} mapped on #{hsink.inspect}"
          pp port_source = get_output(hsource)

          case hsink
          when HW::Reg, HW::StatusReg
            reg=hsink
            # on which input mux input should I connect ?
            d_input   = get_input(reg,0)
            # port_sink is then the D input of the Reg.
            mux=d_input.get_front_mux()  # mux in front of D
            mux_input=mux.get_input_fed_by(port_source) || mux.add_input(var_node.ast.type)
            datapath.connect port_source,mux_input
            mux_idx=mux.input_index(mux_input)
            edge.infos[:transfer]={mux=>mux_idx}
          when HW::Alu
            alu=hsink
            # on which input mux input should I connect ?
            operand_idx=sink.pred_edges.index{|edge| edge.source==source}
            port_operand   = get_input(alu,operand_idx)
            mux=port_operand.get_front_mux()
            mux_input=mux.get_input_fed_by(port_source) || mux.add_input(mux.type)
            datapath.connect port_source,mux_input
            mux_idx=mux.input_index(mux_input)
            edge.infos[:transfer]={mux=>mux_idx}
          when HW::Output
            output=hsink
            mux=output.get_front_mux()
            mux_input=mux.get_input_fed_by(port_source) || mux.add_input(var_node.ast.type)
            datapath.connect port_source,mux_input
            mux_idx=mux.input_index(mux_input)
            edge.infos[:transfer]={mux=>mux_idx}
          when HW::Wire
            port_sink   = get_input(hsink,0)
            datapath.connect port_source,port_sink
          when HW::Ram
            memaccess=hsink
            operand_idx=sink.pred_edges.index{|edge| edge.source==source}
            port=get_input(hsink,operand_idx)
            mux=port.get_front_mux()
            type=memaccess.data_type
            pp mux_input=mux.get_input_fed_by(port_source) || mux.add_input(type)
            datapath.connect port_source,mux_input
            mux_idx=mux.input_index(mux_input)
            edge.infos[:transfer]={mux=>mux_idx}
          else
            raise "unknown hardware sink : #{hsink.inspect}"
          end
          #step
        end
      end
    end

    def simplify cdfg,datapath
      log "      |-->[+] simplify datapath"
      simplify_interconnect(cdfg,datapath)
      suppress_obsolete_transfer(cdfg,datapath)
    end

    def simplify_interconnect cdfg,datapath
      log "           |-->[+] simplify interconnect"
      cands=datapath.components.select{|comp| comp.is_a?(HW::Mux) or comp.is_a?(HW::Wire)}
      cands=cands.select{|mux| mux.inputs.size==1}
      cands.each do |cand|
        log_dbg "simplifying #{cand.inspect}".center(40,'-')
        input=cand.inputs.first
        wire_1=input.pred_edges.first
        source=wire_1.source
        output=cand.output
        wire_2=output.succ_edges.first
        sink=wire_2.sink
        datapath.deconnect source,input
        datapath.deconnect output,sink
        datapath.connect source,sink
        datapath.delete_component(cand)
      end
    end

    def suppress_obsolete_transfer cdfg,datapath
      log "           |-->[+] suppress obsolete transfers"
      nb_suppressed=0
      cdfg.each do |bb|
        bb.dfg.each_edge do |edge|
          if transfer=edge.infos[:transfer]
            hw,val=transfer.first
            unless datapath.components.include?(hw)
              edge.infos[:transfer]=nil
              nb_suppressed+=1
            end
          end
        end
      end
      log "                |--> # suppressed : #{nb_suppressed}"
    end

    def name_signals datapath
      log "|-->[+] name signal datapath".rjust(34)
      n=0
      @signals={}
      datapath.each_edge do |edge|
        source=edge.source
        component=source.component
        comp_name=component.name.to_s+"_" unless (component==datapath)
        name="#{comp_name}#{edge.source.name}".downcase
        type=edge.source.type
        signal=(@signals[name]||=Signal.new(name,type))
        edge.infos[:signal]=signal
        n+=1
      end
      log "     |--> # signal created : #{n}".rjust(37)
    end

    def clean_transfers cdfg
      #needed after MUX suppression / simplify
      muxes=cdfg.datapath.components.select{|comp| comp.is_a? HW::Mux}
      nb_clean=0
      cdfg.each do |bb|
        bb.dfg.each_edge do |edge|
          xfer=edge.infos[:transfer]
          if cmd=xfer.mux_prod_cmd
            mux,value=cmd.first
            unless muxes.include? mux
              nb_clean+=1
              edge.infos[:transfer].mux_prod_cmd.delete(mux)
            end
          end
          if cmd=xfer.mux_cons_cmd
            mux,value=cmd.first
            unless muxes.include? mux
              nb_clean+=1
              edge.infos[:transfer].mux_cons_cmd.delete(mux)
            end
          end
        end
      end
      log_dbg "#{nb_clean} transfers cleant"
    end

    def create_controler cdfg
      log " |-->[+] creating controler"
      @controler=Controler.new(cdfg.name)

      create_bb_states(cdfg)
      link_bb_states(cdfg)

      @controler.save_as "#{@controler.name}_controler.dot"
      return @controler
    end

    def create_bb_states cdfg
      cdfg.each do |bb|
        log "      |-->[+] dealing with basic block #{bb.name}"
        prev=nil
        scheduling=bb.dfg.nodes.group_by{|node| node.cstep}
        scheduling.each do |cstep,same_cstep_nodes|
          puts "#{cstep}".center(40,'-')
          pp same_cstep_nodes
          current_state=State.new("state_#{cstep}_#{bb.name}")
          unless cstep
            puts "ERROR : some nodes are not scheduled !"
            pp same_cstep_nodes
          end
          @controler.add_bb_state(bb,current_state)
          prev.to(current_state) if prev
          insert_transfers(current_state,cstep,same_cstep_nodes)
          insert_activations(current_state,cstep,same_cstep_nodes)
          prev=current_state
        end
      end
    end

    def insert_transfers state,cstep,same_cstep_nodes
      same_cstep_nodes.each do |node|
        node.pred_edges.each do |edge|
          if edge.sink.cstep==cstep
            transfer=edge.infos[:transfer]
            state.add_control(transfer) if transfer
          end
        end
        node.succ_edges.each do |edge|
          if edge.sink.cstep==cstep
            transfer=edge.infos[:transfer]
            state.add_control(transfer) if transfer
          end
        end
      end
    end

    def insert_activations state,cstep,same_cstep_nodes
      insert_alus_activations(state,cstep,same_cstep_nodes)
      insert_rams_activations(state,cstep,same_cstep_nodes)
    end

    def insert_alus_activations state,cstep,same_cstep_nodes
      op_nodes=same_cstep_nodes.select{|node| node.mapping.is_a?(HW::Alu)}
      alus=op_nodes.map{|n| n.mapping}
      alus.each do |alu|
        op=alu.infos[:activations][cstep]
        state.add_control(alu=>op) if op
      end
    end

    def insert_rams_activations state,cstep,same_cstep_nodes
      mem_nodes=same_cstep_nodes.select{|node| node.mapping.is_a?(HW::Ram)}
      rams=mem_nodes.map{|n| n.mapping}
      rams.each do |ram|
        op=ram.infos[:activations][cstep]
        state.add_control(ram=>op) if op
      end
    end

    def link_bb_states cdfg
      log "|-->[+] linking states".rjust(28)
      cdfg.each do |bb|
        curr_bb_state_end  =@controler.states_h[bb].last
        branch=bb.dfg.nodes.find{|node| (ast=node.ast).is_a? Branch}
        stop  =bb.dfg.nodes.find{|node| (ast=node.ast).is_a? Stop}
        if branch
          cstep=branch.cstep
          case (ite=goto=branch).ast
          when Ite
            status_reg=ite.mapping
            state_ite_t=@controler.states_h[ite.ast.trueBranch].first
            state_ite_f=@controler.states_h[ite.ast.falseBranch].first
            control=CtrlIte.new(status_reg,state_ite_t,state_ite_f)
            curr_bb_state_end.add_branch(control)
            curr_bb_state_end.to(state_ite_t, branch:"#{status_reg.inspect}")
            curr_bb_state_end.to(state_ite_f, branch:"!#{status_reg.inspect}")
          when Goto
            goto_state=@controler.states_h[ite.ast.succs.first].first
            curr_bb_state_end.to goto_state
          end
        elsif stop
          log_dbg "stop #{stop} found"
          curr_bb_state_end.add_done!
        end
      end
      log "|--> #states".rjust(23)+"#{@controler.states.size}".rjust(45,'.')
    end

  end

end
