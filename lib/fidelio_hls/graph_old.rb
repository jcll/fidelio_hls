require_relative "code"

module CDFGLang

  class Graph
    attr_accessor :name,:nodes
    def initialize name
      @name=name
      @nodes=[]
    end

    def << node
      @nodes << node
    end

    def size
      @nodes.size
    end

    def connect source,sink,infos={}
      raise "source nil" unless source
      raise "sink   nil" unless sink
      @nodes << source unless @nodes.include? source
      @nodes << sink   unless @nodes.include? sink
      source.to(sink)
    end

    def unconnect source,sink
      source.succs.delete(sink)
      sink.preds.delete(source)
    end

    def delete node
      node.preds.each{|pred| unconnect(pred,node)}
      node.succs.each{|succ| unconnect(node,succ)}
    end

    def each_node &block
      @nodes.each(&block)
    end

    def each_edge &block
      edges.each(&block)
    end

    def edges
      @nodes.collect do |n|
        n.succs.collect{|succ| Edge.new(n,succ)}
      end.flatten
    end

    def to_dot
      @dot_code=Code.new
      @dot_code << "digraph #{name} {"
      @dot_code.indent=2

      each_node do |node|
        @dot_code << "#{node.object_id};"
      end
      each_edge do |edge|
        @dot_code << "#{edge.source.object_id} -> #{edge.sink.object_id};"
      end
      @dot_code.indent=0
      @dot_code << "}"
      return @dot_code
    end

    def save_as dotfilename
      @dot_code||=to_dot()
      @dot_code.save_as dotfilename
    end
  end

  class Node
    attr_accessor :infos
    attr_accessor :succs,:preds
    def initialize infos={}
      @infos=infos
      @succs=[]
      @preds=[]
    end

    def to node
      @succs << node
      node.preds << self
    end
  end

  class Edge
    attr_accessor :name,:source,:sink
    def initialize source,sink
      @source=source
      @sink=sink
    end
  end
end
