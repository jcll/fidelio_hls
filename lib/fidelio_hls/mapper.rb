require_relative "archi"
require_relative "archi_resources"

module FidelioHLS

  class Mapper

    include Log

    attr_accessor :datapath

    def map cdfg
      @pool=HW::ResourcePool.new
      @var_to_reg={}
      log "[+] allocating resources"
      cdfg.datapath=@datapath=Datapath.new(cdfg.name)
      @mapping={}
      cdfg.each{|bb| allocate_bb(bb)}
    end

    def allocate_bb bb,algo=:simple
      log " |-->[+] allocation [#{algo}] #{bb.name}"
      map_ports bb
      map_operations bb
      map_ite bb
      map_seq_vars bb
      map_comb_vars bb
      map_const bb
      map_mem_nodes bb
    end

    def io_kind node
      node.ast.class.to_s.split("::").last.downcase.to_sym
    end

    def map_ports bb
      ios=bb.dfg.select{|n| n.ast.is_a? Io}
      log "      |-->[+] ports     allocation" if ios.any?
      input_id,output_id=-1,-1
      ios.each do |io|
        #log "mapping io #{io.ast.sexp}"
        io_kind,cstep=io_kind(io),io.cstep
        ports=@pool.get_ports_for(io.ast)
        if ports.any?
          cand_ports=ports.select{|port| !port.occupied_at[cstep]}
          if cand_ports.any?
            port=cand_ports.first
          else
            case io_kind
            when :read
              input_id+=1
              name="I#{input_id}"
              @pool.allocate port=HW::Input.new(datapath,name,:int8)
            when :write
              output_id+=1
              name="O#{output_id}"
              @pool.allocate port=HW::Output.new(datapath,name,:int8)
            end
          end
        else
          case io_kind
          when :read
            input_id+=1
            name="I#{input_id}"
            @pool.allocate port=HW::Input.new(datapath,name,:int8)
          when :write
            output_id+=1
            name="O#{output_id}"
            @pool.allocate port=HW::Output.new(datapath,name,:int8)
          end
        end
        log "|-->[+]".rjust(18)+ " cstep #{cstep} : node #{io.ast.sexp}".ljust(50,'.')+ " mapped on #{port.inspect}"
        port.occupied_at[cstep]=io
        @mapping[io]=port
        io.map_on port
      end

    end

    def map_operations bb
      op_nodes=bb.dfg.select{|n| n.is_a? ComputeNode}
      log "      |-->[+] operators allocation" if op_nodes.any?
      op_nodes.sort_by!{|n| n.cstep}
      op_nodes.each do |node|
        log_dbg "mapping #{node.ast.sexp}" #if $options[:verbose]
        op,cstep=node.op,node.cstep
        alus=@pool.get_alus
        log_dbg "#alus=%d" % alus.size
        if alus.any?
          comp_alus=@pool.get_alus_for(op)
          log_dbg "#compatible alus     =%d" % comp_alus.size
          if comp_alus.empty?
            alu=alus.sample # instead of first
            alu.add_operation op
            log_dbg "#{alu.name} can now do ops : #{alu.operations}"
          end
          comp_alus=@pool.get_alus_for(op) # again
          log_dbg "#compatible alus     =%d" % comp_alus.size
          free_alus=comp_alus.reject{|alu| alu.occupied_at[cstep]}
          log_dbg "#free compatible alus=%d" % free_alus.size
          if free_alus.any?
            log_dbg "free alus : #{free_alus.map{|alu| alu.inspect}}"
            alu=free_alus.sample
          else
            #create
            @pool.allocate alu=HW::Alu.new(op)
            log_dbg "created ALU : #{alu.inspect}"
          end
        else
          @pool.allocate alu=HW::Alu.new(op)
          log_dbg "created ALU : #{alu.inspect}"
        end
        log "|-->[+]".rjust(18)+ " cstep #{cstep} : node #{node.ast.sexp}".ljust(50,'.')+ " mapped on #{alu.inspect}"
        (alu.infos[:activations]||={})
        alu.infos[:activations][cstep]=node.ast.op
        alu.occupied_at[cstep]=node
        @mapping[node]=alu
        node.map_on alu
      end
    end

    def map_ite bb
      ite=bb.dfg.nodes.find{|n| n.ast.is_a?(Ite)}
      ite.map_on sb=HW::StatusReg.new(ite.ast.cond) if ite
    end

    # no dataflow analysis until now. each user var is assigned to a reg :-(
    def map_user_var_nodes user_var_nodes
      log "           |-->[+] allocating users var nodes" if user_var_nodes.any?
      user_var_nodes.each do |var_node|
        var=var_node.ast
        unless reg=@mapping[var.name]
          @pool.allocate reg=HW::Reg.new(:int8,resusable=false)
        end
        map_var_on_reg(var_node,reg)
      end
    end

    def map_var_on_reg var_node,reg
      var=var_node.ast
      var_node.map_on reg
      @mapping[var.name]=reg
      log "|-->[+]".rjust(23)+ " var #{var.sexp}".ljust(45,'.')+ " mapped on #{reg.inspect}"
    end

    def map_seq_vars bb
      log "      |-->[+] registers allocation"
      data_nodes=bb.dfg.select{|n| n.is_a? DataNode}
      #---
      user_var_nodes=data_nodes.select{|n| n.ast.name.is_a? Symbol}
      map_user_var_nodes(user_var_nodes)
      #---
      data_nodes=data_nodes.reject{|n| n.pred.nil? or n.succ.nil?}
      assignable_vars=data_nodes.select{|dnode| dnode.pred.cstep != dnode.succ.cstep}

      assignable_vars.each do |dnode|
        #step
        var=dnode.ast
        vname=var.name
        cstep_s=dnode.pred.cstep+1
        cstep_e=dnode.succ.cstep
        interval=cstep_s..cstep_e
        unless reg=@mapping[var.name] #var object may be already mapped to a (definitive!) register
          log "           |-->[+] register allocation for '#{var.name}'"
          regs=@pool.get_regs_for(var)
          unless regs.any?
            @pool.allocate reg=HW::Reg.new(:int8)
          else
            cand_regs=regs.select{|reg| reg.free_during?(interval)}
            log_dbg "candidates regs for #{var.name} : #{cand_regs.collect{|reg| reg.inspect}}"
            if cand_regs.any?
              reg=cand_regs.first
            else
              @pool.allocate reg=HW::Reg.new(:int8)
            end
          end
          log "|-->[+]".rjust(23)+ " csteps #{interval} : var #{var.sexp}".ljust(45,'.')+ " mapped on #{reg.inspect}"
          @mapping[var.name]=reg
        end
        dnode.map_on reg
        interval.each{|cstep| reg.occupied_at[cstep]=dnode} #BUG fix
      end
    end

    def map_comb_vars bb
      log "      |-->[+] wires allocation"
      data_nodes=bb.dfg.select{|n| n.is_a? DataNode}
      data_nodes=data_nodes.reject{|n| n.pred.nil? or n.succ.nil?}
      comb_vars=data_nodes.select{|dnode| dnode.pred.cstep == dnode.succ.cstep}
      comb_vars.each do |dnode|
        var=dnode.ast
        @mapping[dnode]=wire=HW::Wire.new(:int8)
        dnode.map_on wire
        log "|-->[+]".rjust(23)+ " var #{var.sexp}".ljust(45,'.')+ " mapped on #{wire.inspect}"
      end
    end

    def map_const bb
      const_nodes=bb.dfg.select{|node| node.is_a? ConstNode}
      const_nodes.each do |node|
        const=node.ast
        node.map_on cst=HW::Cst.new(const.type,const)
        @mapping[node]=cst
      end
    end

    def map_mem_nodes bb
      log "      |-->[+] rams access allocation"
      mem_nodes=bb.dfg.select{|n| n.is_a? MemNode}
      mem_nodes.each do |mem_node|
        mem=mem_node.ast.mem
        cstep=mem_node.cstep
        unless mem_access=@mapping[mem.name]
          @pool.allocate mem_access=HW::Ram.new(addr_type=:int8,data_type=:int8)
        end
        map_on_mem_access(mem_node,mem_access)
        # controler commands :
        rw=mem_node.ast.is_a?(MWrite) ? :RAM_WRITE : :RAM_READ
        (mem_access.infos[:activations]||={})
        mem_access.infos[:activations][cstep]=rw
        mem_access.occupied_at[cstep]=mem_node
      end
    end

    def map_on_mem_access node,memacc
      mem_instr =node.ast
      mem_name=mem_instr.mem.name
      node.map_on memacc
      @mapping[mem_name]=memacc
      log "|-->[+]".rjust(23)+ " mem node #{mem_instr.sexp}".ljust(45,'.')+ " mapped on #{memacc.inspect}"
    end
  end
end
