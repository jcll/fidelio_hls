require 'sxp'
require_relative 'ast'

module FidelioHLS

  class Parser

    include Log

    attr_accessor :options

    def initialize
      @type_table={}
      @var_table ={}
    end

    def parse sexpfile
      log "[+] parsing  '#{sexpfile}'"
      sexp=SXP.read IO.read(sexpfile)
      ast=objectify(sexp)
    end

    def objectify sexp
      cdfg=CDFG.new
      sexp.shift
      cdfg.name=sexp.shift
      while sexp.any?
        s=sexp.first
        case s.first
        when :type
          cdfg.type_defs << parse_type_def(sexp.shift)
        when :var
          cdfg.var_defs << parse_var_def(sexp.shift)
        when :basicblock
          cdfg.bbs << parse_bb(sexp.shift)
        end
      end
      cdfg
    end

    def expect sexp,klass,value=nil
      unless (kind=sexp.shift).is_a? klass
        log "ERROR : expecting a #{klass}. Got a #{kind}."
        raise "Syntax error"
      end
      if value
        unless value==kind
          log "ERROR : expecting value '#{value}'. Got '#{kind}'"
          raise "Syntax error"
        end
      end
      return kind
    end

    #======== declaration stuff
    def parse_type_def sexp
      type_def=TypeDef.new
      sexp.shift
      type_def.name=sexp.shift
      type_def.definition=parse_type(sexp.shift)
      type_def
    end

    def parse_var_def sexp
      var_def=VarDef.new
      sexp.shift
      var_def.name=sexp.shift
      var_def.type=parse_type(sexp.shift)
      var_def
    end

    def parse_type sexp
      case sexp
      when Array
        expect(sexp,Symbol,:array)
        type=ArrayType.new
        type.size=sexp.shift
        type.element_type=parse_type(sexp.shift)
      when Symbol
        case sexp
        when :int8,:uint8,:int16,:uint16,:int32,:uint32
          type=BasicType.new
          type.name=sexp
        else
          type=UnknownType.new
          type.name=sexp
        end
      end
      type
    end
    #========================================
    def parse_bb sexp
      bb=BasicBlock.new
      expect(sexp,Symbol,:basicblock)
      bb.name=sexp.shift
      while sexp.any?
        bb.stmts << parse_stmt(sexp.shift)
      end
      bb
    end

    def parse_stmt sexp
      car=sexp.first
      case car
      when :read
        parse_read sexp
      when :write
        parse_write sexp
      when :mwrite
        parse_mwrite sexp
      when :assign
        parse_assign sexp
      when :goto
        parse_goto sexp
      when :ite
        parse_ite sexp
      when :stop
        parse_stop sexp
      when :puts
        parse_puts sexp
      else
        raise "unknown stmt '#{sexp}'"
      end
    end

    def parse_mwrite sexp
      sexp.shift
      ret=MWrite.new
      ret.mem   = Mem.new(sexp.shift)
      ret.addr  = parse_expression sexp.shift
      ret.expr  = parse_expression sexp.shift
      ret
    end

    def parse_read sexp
      sexp.shift
      ret=Read.new
      ret.var   = parse_expression sexp.shift
      ret.port  = Port.new(sexp.shift)
      ret
    end

    def parse_write sexp
      sexp.shift
      ret=Write.new
      ret.var   = parse_expression sexp.shift
      ret.port  = Port.new(sexp.shift)
      ret
    end

    def parse_assign sexp
      sexp.shift
      ret=Assign.new
      ret.lhs=parse_expression(sexp.shift)
      ret.rhs=parse_expression(sexp.shift)
      ret
    end

    def parse_goto sexp
      sexp.shift
      Goto.new(sexp.shift)
    end

    def parse_ite sexp
      ret=Ite.new
      sexp.shift
      ret.cond=parse_expression sexp.shift
      ret.trueBranch= sexp.shift
      ret.falseBranch= sexp.shift
      ret
    end

    def parse_stop sexp
      Stop.new
    end

    def parse_puts sexp
      ret=Puts.new
      sexp.shift
      ret.str=sexp.shift
      ret
    end

    # expressions

    def parse_expression sexp
      case sexp
      when Array
        case sexp.size
        when 1
          case sexp
          when Symbol
            return Var.new(sexp)
          when Integer
            return Const.new(sexp)
          else
            raise "unknown expression '#{sexp}'"
          end
        when 2
          ret=parse_unary(sexp)
        when 3
          ret=parse_binary(sexp)
        else
          raise "unknown expression '#{sexp}'(size = '#{sexp.size}')"
        end
      when Symbol
        ret=Var.new(sexp)
      when Integer
        ret=Const.new(sexp)
      else
        raise "unknown expression '#{sexp}'"
      end
      ret
    end

    OP_TRANSLATE={
      "gt" => ">",
      "lt" => "<",
      "eq" => "=",
      "neq"=> "/=",
      "gte"=> ">=",
      "lte"=> "<=",
    }

    NORMALIZED_OP={
      "*"  => "mul",
      "+"  => "add",
      "-"  => "sub",
      "/"  => "div",
      "="  => "eq",
      ">=" => "gte",
      "<=" => "lte",
      "/=" => "neq"
    }

    def parse_binary sexp
      case sexp.first
      when :mread
        ret=MRead.new
        sexp.shift
        ret.mem = Mem.new(sexp.shift) # memory name
        ret.addr= parse_expression(sexp.shift)
      else
        ret=Binary.new
        ret.op=sexp.shift
        #ret.op=OP_TRANSLATE[ret.op.to_s]||ret.op.to_s
        ret.op=NORMALIZED_OP[ret.op.to_s] || ret.op.to_s
        ret.lhs=parse_expression(sexp.shift)
        ret.rhs=parse_expression(sexp.shift)
      end
      ret
    end

    def parse_unary sexp
      ret=Unary.new
      ret.op=sexp.shift
      ret
    end
  end
end
