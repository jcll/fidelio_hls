require_relative "ast_sexp_hw"

module FidelioHLS
  class Reporter
    include Log
    def report cdfg
      log "[+] reporting solution :"
      puts (code=cdfg.sexp_hw).finalize
      code.save_as "#{cdfg.name}.cdfg_hw"
    end
  end
end
