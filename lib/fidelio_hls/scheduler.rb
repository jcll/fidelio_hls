module FidelioHLS

  class Scheduler
    include Log

    def schedule cdfg
      log  "[+] scheduling"
      @csteps={} #final scheduling for CDFG
      @max_cstep=0
      cdfg.each{|bb|
        log "scheduling #{bb.name}".center(40,'=')
        schedule_bb(bb)
      }
      print_scheduling(@csteps)
      cdfg.draw "#{cdfg.name}_cdfg_scheduled.dot"
      return @csteps
    end

    def schedule_bb bb,algo=:asap
      log  " |-->[+] scheduling [#{algo}] #{bb.name}"
      dfg=bb.dfg
      case algo
      when :asap
        csteps_dfg=asap(dfg)
      end
      merge_dfg_cstep(csteps_dfg)
    end


    def merge_dfg_cstep dfg_cstep
      offset=@csteps.keys.size
      dfg_cstep.each do |cstep,nodes|
        merged_cstep=offset+cstep
        @csteps[merged_cstep]=nodes
        nodes.each{|node| node.cstep=merged_cstep}
      end
    end

    def sched_at n,cstep
      unless @cstep_for[n]
        log "sched_at : cstep #{cstep} : #{n.inspect}"
        @cstep_for[n]=cstep
        @candidates.delete(n)
      else
        log "sched_at : #{n.inspect} already scheduled"
      end
    end

    def asap_init dfg
      log "asap init..."
      first_nodes=dfg.select{|n| n.preds.empty?}.reject{|n| n.is_a? ControlNode}.uniq
      first_nodes.each{|n|
        sched_at(n,0)
        n.succs.each{|succ|
          if all_scheduled?(succ.preds)
            sched_at(succ,0)
          end
        }
      }
    end

    def asap dfg
      @cstep_for={}
      max_cstep=0
      @candidates=[dfg.nodes].flatten
      cnode=@candidates.find{|n| n.is_a? ControlNode}
      @candidates.delete(cnode)
      asap_init(dfg)
      cstep=0
      while @candidates.any?
        cand=@candidates.shift
        log "trying to schedule #{cand.inspect}"
        unless @cstep_for[cand]
          preds=cand.preds
          if preds.any?
            if all_scheduled?(preds)
              puts "all preds sched"
              max_preds=preds.map{|n| @cstep_for[n]}.max
              pp cand
              pp cand.class
              case cand
              when ComputeNode
                delay=1 #Unit delay for all "D"ata operator.
              when MemNode
                delay=1
              when DataNode
                if cand.pred.is_a?(MemNode) and cand.pred.ast.is_a?(MRead)
                  delay=1 # output data available 1 cycle later.
                else
                  delay=0
                end
                puts "data node -> #{delay}"
              else
                delay=0
              end
              cstep=max_preds+delay
              puts "cstep : #{cstep}"
              sched_at(cand,cstep)
              if cand.is_a? ComputeNode #propagate to DataNode
                cand.succs.each{|succ| sched_at(succ,cstep)}
              end
              max_cstep=[max_cstep,cstep].max
              log_dbg "=> max_cstep=#{max_cstep}"
            else
              candidates << cand
            end
          else
            sched_at(cand,0)
          end
        end
      end

      sched_at(cnode,max_cstep) if cnode

      #inverse hash
      hash=compute_csteps()
      return hash
    end

    def compute_csteps
      csteps={}
      @cstep_for.each{|node,step|
        csteps[step]||=[]
        csteps[step] << node
        node.cstep=step
      }
      return csteps
    end

    def all_scheduled? nodes
      status=nodes.map{|node| @cstep_for[node]}
      return false if status.include? nil
      true
    end

    def print_scheduling csteps
      log " |-->[+] schedule found : "
      log  "-"*80
      log  "CSTEP |"+"NODES".center(80)
      log  "-"*80
      for cstep in csteps.keys.sort
        nodes=csteps[cstep]
        sexp=nodes.map{|n| n.infos[:ast].sexp}.join(" ")
        log  "#{cstep.to_s.rjust(5)} | #{sexp}"
      end
      log  "-"*80
    end
  end
end
