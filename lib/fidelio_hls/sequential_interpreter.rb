require_relative 'visitor'

module FidelioHLS

  class SequentialInterpreter < Visitor

    include Log

    def run cdfg
      puts "[+] running interpreter"
      @symval={}
      @mems={}
      cdfg.accept(self)
    end

    def visitCDFG cdfg,args=nil
      indent 0,cdfg.name
      cdfg.bbs.each{|bb| bb.accept(self,args)}
  	end

    def visitBasicBlock bb,args=nil
      indent 2,bb.name
      bb.stmts.each{|stmt| stmt.accept(self,args)}
  	end

    # stmts
    def visitRead rd,args=nil
      raise "NIY : read port"
      indent 3,rd.sexp
      port=rd.port.accept(self,args)
      var=rd.var.accept(self,args)
  	end

    def visitWrite node,args=nil
      raise "NIY : write port"
      indent 3,node.sexp
      rd.var.accept(self,args)
      rd.port.accept(self,args)
  	end

    def visitStop node,args=nil
      raise "STOP : visit stopped"
  	end

    def visitAssign assign,args=nil
      indent 3,assign.sexp
      rhs=assign.rhs.accept(self)
      var=assign.lhs.accept(self,:assign)
      @symval[var.name]=rhs
      pp @symval
  	end

    def visitGoto goto,args=nil
      indent 3,goto.sexp
      goto.succ.accept(self)
  	end

    def visitIte ite,args=nil
      indent 3,ite.sexp
      cond=ite.cond.accept(self,args)
      if cond
        ite.trueBranch.accept(self,args)
      else
        ite.falseBranch.accept(self,args)
      end
  	end

    def visitMWrite mw,args=nil
      indent 3,mw.sexp
      mw.addr.accept(self,args)
      mw.expr.accept(self,args)
      mw.mem.accept(self,args)
  	end

    # expressions
    def visitMRead mr,args=nil
      indent 4,mr.sexp
      addr=mr.addr.accept(self,args)
      mem=mr.mem.accept(self,:use)
      return mem[addr]
  	end

    def visitBinary binary,args=nil
      indent 4,binary.sexp
      rhs=binary.rhs.accept(self)
      op=binary.op
      lhs=binary.lhs.accept(self)
      case op
      when "add"
        ret=lhs + rhs
      when "sub"
        ret=lhs - rhs
      when "mul"
        ret=lhs * rhs
      when "div"
        ret=lhs / rhs
      when "lt"
        ret=lhs < rhs
      when "lte"
        ret=lhs <= rhs
      else
        raise "NIY : #{op}"
      end
      puts "? (#{lhs} #{op} #{rhs}) -> #{ret}"
      ret
  	end

    def visitUnary unary,args=nil
      indent 4,unary.sexp
      unary.expr.accept(self,args)
  	end

    def visitConst const,args=nil
      indent 4,const.sexp
      const.val
  	end

    def visitVar var,use_or_assign
      indent 4,var.sexp
      case use_or_assign
      when :assign
        return var
      else
        if val=@symval[var.name]
          return val
        else
          raise "ERROR : var #{var.name} is undefined !"
        end
      end
  	end

    def visitPort port,args=nil
      indent 4,port.sexp
      port
  	end

    def visitMem mem,args=nil
      indent 4,mem.sexp
      @mems[mem.name] ||=[]
  	end

  end
end
