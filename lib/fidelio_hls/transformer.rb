module FidelioHLS

  class Transformer < Visitor

    def visitCDFG cdfg,args=nil
      ret=CDFG.new
      ret.type_defs= cdfg.type_defs.map{|td| td.accept(self,args)}
      ret.var_defs = cdfg.var_defs.map {|vd| vd.accept(self,args)}
      ret.bbs      = cdfg.bbs.map{|bb| bb.accept(self,args)}
      ret
  	end

    def visitTypeDef td,args=nil
      td.name #Symbol
      td.definition.accept(self,args)
    end

    def visitVarDef vd,args=nil
      vd.name #Symbol
      vd.type.accept(self)
    end

    def visitBasicType bt,args=nil
      bt.name
    end

    def visitUnknownType bt,args=nil
      bt.name
    end

    def visitArrayType at,args=nil
      at.size #Integer
      at.element_type.accept(self,args)
    end

    #==============================
    def visitBasicBlock bb,args=nil
      pp bb
      ret=BasicBlock.new
      ret.bb=bb.stmts.map{|stmt| stmt.accept(self,args)}
  	end

    def visitRead rd,args=nil
      rd.port.accept(self,args)
      rd.var.accept(self,args)
  	end

    def visitWrite wr,args=nil
      wr.var.accept(self,args)
      wr.port.accept(self,args)
  	end

    def visitStop node,args=nil
  	end

    def visitAssign assign,args=nil
      assign.rhs.accept(self)
      assign.lhs.accept(self)
  	end

    def visitGoto goto,args=nil
      goto.succ #Symbol
  	end

    def visitIte ite,args=nil
      ite.cond.accept(self,args)
      ite.trueBranch #Symbol
      ite.falseBranch #Symbol
  	end

    def visitMWrite mw,args=nil
      mw.addr.accept(self,args)
      mw.expr.accept(self,args)
      mw.mem.accept(self,args)
  	end

    def visitPuts puts_,args=nil
      puts_.str
    end

    # expressions
    def visitMRead mr,args=nil
      mr.addr.accept(self,args)
      mr.mem.accept(self,args)
  	end

    def visitBinary binary,args=nil
      binary.rhs.accept(self,args)
      binary.lhs.accept(self,args)
  	end

    def visitUnary unary,args=nil
      unary.expr.accept(self,args)
  	end

    def visitConst const,args=nil
      const
  	end

    def visitVar var,args=nil
      var
  	end

    def visitPort port,args=nil
      port
  	end

    def visitMem mem,args=nil
      mem
  	end

  end
end
