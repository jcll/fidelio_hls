
module VHDL
  Model        = Struct.new(:name,:header,:entity,:archi)
  Entity       = Struct.new(:name,:generics,:ports)
  Port         = Struct.new(:name,:type)

  class Input < Port
  end
  class Output < Port
  end

  Architecture = Struct.new(:name,:entity,:decls,:body)
  Signal       = Struct.new(:name,:type,:init)
  Instance     = Struct.new(:name,:lib,:entity_name,:arch_name,:generic_map,:port_map)

end
