require_relative 'code'

module FidelioHLS

  class VHDLGenerator
    include Log

    CODEOP={
        "nop"  => "OP_NOP",
        "add"  => "OP_ADD",
        "sub"  => "OP_SUB",
        "mul"  => "OP_MUL",
        "div"  => "OP_DIV",
        "gt"   => "OP_GT",
        "lt"   => "OP_LT",
        "eq"   => "OP_EQ",
        "neq"  => "OP_NEQ",
        "gte"  => "OP_GTE",
        "lte"  => "OP_LTE",
    }

    def initialize
      @types_h={} # IR to VHDL syntax mapping.
      @basic_components=[]
      @basic_type_names=[]
    end

    # ============= CODE GENERATION =============
    def generate fsmd
      puts "[+] VHDL generation"
      @vhdl_files=[]
      gen_controler(fsmd)
      gen_datapath(fsmd)
      gen_fsmd(fsmd)
      gen_type_package(fsmd)
      gen_tb(fsmd)
      gen_fidelio_components(fsmd)
      gen_fidelio_type_package(fsmd)
      gen_ghdl_script(fsmd)
      sanitize
    end

    private
    # ============ types mapping ================
    def register_type ir_type
      @types_h[ir_type]||=translate_to_vhdl_type(ir_type)
    end

    def translate_to_vhdl_type ir_type
      ir_type=ir_type.to_s
      case ir_type
      when /int(\d?)/
        d=$1.to_i - 1
        return "signed(#{d} downto 0)"
      when /uint(\d?)/
        d=$1.to_i - 1
        return "unsigned(#{d} downto 0)"
      else
        puts "ERROR : unknown type #{ir_type} for VHDL ! : '#{ir_type}'"
        return "ERROR type '#{ir_type}'"
      end
    end

    def translate_to_vhdl_value value,ir_type
      ir_type=ir_type.to_s
      case ir_type
      when /int(\d?)/
        nb_bits=$1
        return "to_signed(#{value},#{nb_bits})"
      when /uint(\d?)/
        nb_bits=$1
        return "to_unsigned(#{value},#{nb_bits})"
      else
        puts "ERROR : unknown type #{ir_type} for VHDL ! : '#{ir_type}'"
        return "ERROR type '#{ir_type}'"
      end
    end

    def default_vhdl_value ir_type
      ir_type=ir_type.to_s
      case ir_type
      when /int(\d?)/
        d=$1.to_i - 1
        return "to_signed(0,#{d+1})"
      when /uint(\d?)/
        d=$1.to_i - 1
        return "to_unsigned(0,#{d+1})"
      else
        puts "ERROR : unknown type #{ir_type} for VHDL ! : '#{ir_type}'"
        return "ERROR type '#{ir_type}'"
      end
    end

    def translate_to_vhdl_op op
      CODEOP[op.to_s]
    end

    def register_component instance
      unless @basic_components.include?(instance)
        @basic_components << instance
      end
    end
    #==============   fidelio ==============
    def gen_fidelio_type_package fsmd
      collect_signal_types(fsmd)
      @vhdl_files.unshift filename="fidelio_type_package.vhd"
      log " |-->[+] type package".ljust(50,'.')+' '+filename
      code=Code.new
      code << header
      code << ieee_header
      code << "package fidelio_type_package is"
      code.indent=2
      code.newline
      code << "constant CTRUE  : std_logic := '1';"
      code << "constant CFALSE : std_logic := '0';"
      code.newline
      code << "constant RAM_DISABLE : std_logic := CFALSE;"
      code << "constant RAM_ENABLE  : std_logic := CTRUE;"
      code << "constant RAM_READ    : std_logic := CFALSE;"
      code << "constant RAM_WRITE   : std_logic := CTRUE;"
      code.newline
      @types_h.each do |ir_type,vhdl_type|
        code << "subtype #{ir_type} is #{vhdl_type};"
      end
      code.newline
      code << "function bool_to_int8(val : boolean) return int8;"
      code.newline
      enum=CODEOP.values.join(',')
      code << "type ALU_OP_TYPE is (#{enum});"
      code.newline
      code.indent=0
      code << "end package;"
      code.newline
      code << "package body fidelio_type_package is"
      code.newline
      code.indent=2
      code << "function bool_to_int8(val : boolean) return int8 is"
      code << "begin"
      code << "  if val then"
      code << "    return to_signed(1,8);"
      code << "  else"
      code << "    return to_signed(0,8);"
      code << "  end if;"
      code << "end;"
      code.indent=0
      code.newline
      code << "end package body;"
      code.save_as filename
    end

    def collect_signal_types fsmd
      fsmd.datapath.signals.each{|sig| @types_h[sig.type]||=translate_to_vhdl_type(sig.type)}
    end

    def fidelio_header
      code=Code.new
      code << "library fidelio_lib;"
      code << "use fidelio_lib.fidelio_type_package.all;"
      code.newline
      code
    end

    def gen_fidelio_components fsmd
      log   " |-->[+] Fidelio RTL components".ljust(50,'.')
      @basic_components.sort_by!{|inst| infer_component_name_from(inst)} #cosmetic
      @basic_components.each do |instance|
        case instance
        when HW::Alu
          gen_fidelio_alu(fsmd,instance)
        else
          gen_fidelio_component(fsmd,instance)
        end
      end
    end

    def gen_fidelio_component fsmd,instance
      name=infer_component_name_from(instance)
      filename="fidelio_#{name}.vhd"
      unless @vhdl_files.include?(filename)
        @vhdl_files.unshift filename
        puts "      |-->[+]".ljust(50,'.')+' '+filename
        code=Code.new
        code << header
        code << ieee_header
        code << fidelio_header
        code << gen_fidelio_component_entity(instance)
        code.newline
        code << gen_fidelio_component_arch(instance)
        code.save_as filename
      end
    end

    def gen_fidelio_alu fsmd,instance
      name=instance.name
      filename="fidelio_#{name}.vhd"
      unless @vhdl_files.include?(filename)
        @vhdl_files.unshift filename
        puts "      |-->[+]".ljust(50,'.')+' '+filename
        code=Code.new
        code << header
        code << ieee_header
        code << fidelio_header
        code << gen_fidelio_component_entity(instance)
        code.newline
        code << gen_fidelio_component_arch(instance)
        code.save_as filename
      end
    end


    def gen_fidelio_component_entity instance
      case alu=mux=reg=ram=instance
      when HW::Mux
        return gen_mux_entity(mux)
      when HW::Reg,HW::StatusReg
        return gen_reg_entity(reg)
      when HW::Alu
        return gen_alu_entity(alu)
      when HW::Ram
        return gen_ram_entity(ram)
      end
    end

    def gen_fidelio_component_arch instance
      name=infer_component_name_from(instance)
      case alu=mux=reg=ram=instance
      when HW::Mux
        code = gen_mux_arch(mux)
      when HW::Reg,HW::StatusReg
        code = gen_reg_arch(reg)
      when HW::Alu
        code = gen_alu_arch(alu)
      when HW::Ram
        code = gen_ram_arch(ram)
      else
        raise "unknown hardware resource."
      end
      code
    end

    def gen_mux_arch mux
      code=Code.new
      name=infer_component_name_from(mux)
      code << "architecture rtl of #{name} is"
      code << "begin --"
      code.indent=2
      if mux.size>0
        code << "with command select"
        str = "F <= "
        for idx in 0..mux.size-1
          str+="I#{idx} when #{idx},"
          code << str
          str="     "
        end
        code << "     I0 when others;"
      else
         code << "F <= (others=>'0');"
      end
      code.indent=0
      code << "end rtl;"
      code
    end

    def gen_reg_arch reg
      code=Code.new
      name=infer_component_name_from(reg)
      code << "architecture rtl of #{name} is"
      code << "begin --"
      code.indent=2
      code << "process(reset_n,clk)"
      code << "begin"
      code.indent=4
      code << "if reset_n='0' then"
      code.indent=6
      code << "q <= #{default_vhdl_value(reg.type)};"
      code.indent=4
      code << "elsif rising_edge(clk) then"
      code.indent=6
      code << "q <= d;"
      code.indent=4
      code << "end if;"
      code.indent=2
      code << "end process;"
      code.indent=0
      code << "end rtl;"
      code
    end

    TO_VHDL={
        "add" => "+"  ,
        "sub" => "-"  ,
        "mul" => "*"  ,
        "div" => "/"  ,
        "gt"  => ">"  ,
        "lt"  => "<"  ,
        "lte" => "<=" ,
        "eq"  => "="  ,
        "neq" => "/=" ,
    }

    def gen_alu_arch alu
      code=Code.new
      name=alu.name
      code << "architecture rtl of #{name} is"
      code << "begin --"
      code.indent=2
      code << "with op select"
      str = "F <= "
      for idx in 0..alu.operations.size-1
        alu_op=alu.operations[idx]
        oper=TO_VHDL[alu_op]
        case oper
        when "+","-","*"
          computation="(I0 #{oper} I1)"
        when "=","/=",">","<","<=",">="
          computation="bool_to_int8(I0 #{oper} I1)"
        else
          raise "(NIY) #{oper} / #{alu_op}"
        end
        operation=CODEOP[alu_op]
        str+="resize(#{computation},8) when #{operation},"
        code << str
        str="     "
      end
      code << "     #{default_vhdl_value(alu.type)} when others;"
      code.indent=0
      code << "end rtl;"
      code
    end

    def gen_ram_arch ram
      code=Code.new
      name=infer_component_name_from(ram)
      data_type=ram.data_type
      code << "architecture rtl of #{name} is"
      code << "  type ram_type is array (0 to 2**nbits_addr-1) of #{data_type};"
      code << "  signal RAM: ram_type;"
      code << "begin"
      code << ""
      code << "  process (clk)"
      code << "  begin"
      code << "    if rising_edge(clk) then"
      code << "      if en = '1' then"
      code << "        if wr = '1' then"
      code << "          RAM(to_integer(unsigned(address))) <= datain;"
      code << "        end if;"
      code << "        dataout <= RAM(to_integer(unsigned(address))) ;"
      code << "      end if;"
      code << "    end if;"
      code << "  end process;"
      code << ""
      code << "end rtl;"
      code
    end

    #============== type package ===========
    def gen_type_package fsmd
      @vhdl_files.unshift filename="#{fsmd.name}_type_package.vhd"
      puts " |-->[+] type package".ljust(50,'.')+' '+filename
      code=Code.new
      code << header
      code << ieee_header
      code << fidelio_header
      code << "package type_package is"
      code.indent=2
      code.newline
      code << gen_control_type(fsmd)
      code.newline
      code << gen_status_type(fsmd)
      code.indent=0
      code.newline
      code << "end package;"
      code.save_as filename
    end

    def gen_control_type fsmd
      controlables=fsmd.datapath.components.select{|comp| comp.is_a?(HW::Mux) or comp.is_a?(HW::Alu) or comp.is_a?(HW::Ram)}
      code=Code.new
      code << "type control_type is record"
      code.indent=2
      controlables.each do |controlable|
        case alu=mux=controlable
        when HW::Mux
          max=mux.size>0 ? (mux.inputs.size-1) : 0 # degenerated MUX (no inputs)
          code << "#{mux.name.downcase} : integer range 0 to #{max};"
        when HW::Alu
          code << "#{alu.name.downcase}     : ALU_OP_TYPE;"
        when HW::Ram
          code << "#{alu.name.downcase}_en : std_logic;"
          code << "#{alu.name.downcase}_wr : std_logic;"
        end
      end
      code.indent=0
      code << "end record;"
      code.newline
      code << "constant CONTROL_INIT : control_type := ("
      code.indent=2
      controlables.each do |controlable|
        case alu=mux=ram=controlable
        when HW::Mux
          code << "#{mux.name.downcase} => 0,"
        when HW::Alu
          code << "#{alu.name.downcase}     => OP_NOP,"
        when HW::Ram
          code << "#{ram.name.downcase}_en => RAM_DISABLE,"
          code << "#{ram.name.downcase}_wr => RAM_DISABLE,"
        end
      end
      code.indent=0
      code << ");"
      code
    end

    def gen_status_type fsmd
      status_regs=fsmd.datapath.components.select{|comp| comp.is_a? HW::StatusReg}
      code=Code.new
      code << "type status_type is record"
      code.indent=2
      code << "dummy : std_logic;"
      status_regs.each do |status_reg|
        code << "#{status_reg.name.downcase} : std_logic;"
      end
      code.indent=0
      code << "end record;"
      code
    end

    def gen_mux_entity mux
      name=infer_component_name_from(mux)
      input_types=mux.inputs.collect{|input| {input.name => input.type}}
      code=Code.new
      code << "entity #{name} is"
      code.indent=2
      code << "port("
      code.indent=4
      code << "command : in integer range 0 to 10;"
      input_types.each do |input_type_h|
        name,type=input_type_h.first
        raise "unknown type for input '#{name}' of #{mux.inspect}" unless type
        code << "#{name} : in  #{mux.type};"
      end
      name,type=mux.output.name,mux.output.type
      code << "#{name} : out  #{type};"
      code.indent=2
      code << ");"
      code.indent=0
      code << "end entity;"
      code
    end

    def gen_reg_entity reg
      cname=infer_component_name_from(reg)
      code=Code.new
      code << "entity #{cname} is"
      code.indent=2
      code << "port("
      code.indent=4
      input_type={reg.input.name => reg.input.type}
      code << clocking
      name,type=input_type.first
      code << "#{name} : in  #{type};"
      name,type=reg.output.name,reg.output.type
      code << "#{name} : out  #{type};"
      code << ");"
      code.indent=0
      code << "end entity #{cname};"
      code
    end

    def gen_alu_entity alu
      cname=alu.name
      input_types=alu.inputs.collect{|input| {input.name => input.type}}
      code=Code.new
      code << "entity #{cname} is"
      code.indent=2
      code << "port("
      code.indent=4
      input_types.each do |input_type_h|
        name,type=input_type_h.first
        code << "#{name} : in  #{type};"
      end
      code << "op : in ALU_OP_TYPE;"
      name,type=alu.output.name,alu.output.type
      code << "#{name} : out  #{type};"
      code.indent=2
      code << ");"
      code.indent=0
      code << "end entity #{cname};"
      code
    end

    def gen_ram_entity ram
      cname=infer_component_name_from(ram)
      data_type=ram.data_type
      code=Code.new
      code << "entity #{cname} is"
      code.indent=2
      code << "generic("
      code.indent=4
      code << "nbits_addr : natural :=8;"
      code.indent=2
      code << ");"
      code << "port("
      code.indent=4
      code << "clk     : in std_logic;"
      code << "en      : in std_logic;"
      code << "wr      : in std_logic;"
      code << "address : in unsigned(nbits_addr-1 downto 0);"
      code << "datain  : in #{data_type};"
      code << "dataout : out #{data_type}"
      code.indent=2
      code << ");"
      code.indent=0
      code << "end entity #{cname};"
    end

    def infer_component_name_from instance
      comp_type=instance.class.to_s.split("::").last.downcase
      output_type=instance.output.type
      case comp_type
      when "alu"
        input_types=instance.inputs.map(&:type).join('_')
        component_name="#{comp_type}_#{input_types}_#{output_type}"
      when "mux"
        component_name="#{comp_type}_#{instance.size}_#{output_type}"
      when "reg"
        component_name="#{comp_type}_#{output_type}"
      when "ram"
        input_types=instance.inputs.map(&:type).join('_')
        component_name="#{comp_type}_#{input_types}"
      else
        raise "error : #{instance} => #{comp_type}"
      end
      component_name
    end

    #=============== FSMD =================
    def gen_fsmd fsmd
      @vhdl_files << filename="#{fsmd.name}_fsmd.vhd"
      puts " |-->[+] fsmd".ljust(50,'.')+' '+filename
      code=Code.new
      code << header
      code << ieee_header
      code << fsmd_header(fsmd)
      code << fsmd_entity(fsmd)
      code << fsmd_archi(fsmd)
      code.save_as filename
    end

    def fsmd_header fsmd
      code=Code.new
      name=fsmd.name
      code << "library fidelio_lib;"
      code << "use fidelio_lib.fidelio_type_package.all;"
      code.newline
      code << "library #{name}_lib;"
      code << "use #{name}_lib.type_package.all;"
      code.newline
      code
    end

    def fsmd_entity fsmd
      code=Code.new
      code << "entity #{fsmd.name}_fsmd is "
      code.indent=2
      code << fsmd_ports(fsmd)
      code.indent=0
      code << "end entity;"
      code.newline
      code
    end

    def fsmd_ports fsmd
      code=Code.new
      code << "port("
      code.indent=2
      code << clocking
      code << "start   : in  std_logic;"
      code << "done    : out std_logic;"
      fsmd.inputs.each{|input|
        code << "#{input.name} : in  #{input.type};"
        register_type(input.type)
      }
      fsmd.outputs.each{|output|
        code << "#{output.name} : out #{output.type};"
        register_type(output.type)
      }
      code.indent=0
      code << ");"
      code
    end

    def fsmd_archi fsmd
      code=Code.new
      code << "architecture rtl of #{fsmd.name}_fsmd is"
      code.indent=2
      code << "signal control : control_type;"
      code << "signal status  : status_type;"
      code.indent=0
      code << "begin"
      code.newline
      code.indent=2
      code << instance_controler(fsmd)
      code.newline
      code << instance_datapath(fsmd)
      code.indent=0
      code.newline
      code << "end rtl;"
      code.newline
      code
    end

    def instance_controler fsmd
      code=Code.new
      code << "fsm : entity #{fsmd.name}_lib.#{fsmd.name}_controler"
      code.indent=4
      code << "port map("
      code.indent=6
      code << clocking_map
      code << "start   => start,"
      code << "done    => done,"
      code << "control => control,"
      code << "status  => status"
      code.indent=4
      code << ");"
      code
    end

    def instance_datapath fsmd
      code=Code.new
      code << "datapath : entity #{fsmd.name}_lib.#{fsmd.name}_datapath"
      code.indent=4
      code << "port map("
      code.indent=6
      code << clocking_map
      code << "control => control,"
      code << "status  => status,"
      fsmd.inputs.each{|input| code << "#{input.name}    => #{input.name},"}
      fsmd.outputs.each{|output| code << "#{output.name} => #{output.name},"}
      code.indent=4
      code << ");"
      code
    end
    #============== controler ==============
    def gen_controler fsmd
      @vhdl_files << filename="#{fsmd.name}_controler.vhd"
      puts " |-->[+] controler".ljust(50,'.')+' '+filename
      code=Code.new
      code << header
      code << ieee_header
      code << fsmd_header(fsmd)
      code << controler_entity(fsmd.controler)
      code << controler_archi(fsmd.controler)
      code.save_as filename
    end

    def controler_entity controler
      code=Code.new
      code << "entity #{controler.name}_controler is "
      code.indent=2
      code << "port("
      code.indent=4
      code << clocking
      code << "start   : in  std_logic;"
      code << "done    : out std_logic;"
      code << "status  : in  status_type;"
      code << "control : out control_type"
      code.indent=2
      code << ");"
      code.indent=0
      code << "end entity;"
      code.newline
      code
    end

    def controler_archi controler
      code=Code.new
      state_list=controler.states.map{|state| state.name}
      state_list.unshift("idle")
      state_list=state_list.join(',')
      code << "architecture rtl of #{controler.name}_controler is"
      code.indent=2
      code << "type state_type is (#{state_list});"
      code << "signal state_c,state_r : state_type;"
      code.newline
      code << "signal control_c,control_r : control_type;"
      code << "signal done_c,done_r       : std_logic;"
      code.indent=0
      code << "begin"
      code.indent=2
      code.newline
      code << state_process()
      code.newline
      code << state_next(controler)
      code.newline
      code << interface_connect()
      code.indent=0
      code.newline
      code << "end rtl;"
      code
    end

    def state_process
      code=Code.new
      code << "state_p: process(reset_n,clk)"
      code << "begin"
      code.indent=2
      code << "if reset_n='0' then"
      code.indent=4
      code << "state_r   <= idle;"
      code << "control_r <= CONTROL_INIT;"
      code << "done_r    <= CFALSE;"
      code.indent=2
      code << "elsif rising_edge(clk) then"
      code.indent=4
      code << "state_r   <= state_c;"
      code << "control_r <= control_c;"
      code << "done_r    <= done_c;"
      code.indent=2
      code << "end if;"
      code.indent=0
      code << "end process;"
      code
    end

    def state_next controler
      code=Code.new
      sensitivity=["start,status,state_r,control_r"]
      code << "next_state_p: process(#{sensitivity.join(',')})"
      code.indent=2
      code << "variable state_v   : state_type;"
      code << "variable control_v : control_type;"
      code << "variable done_v    : std_logic;"
      code.indent=0
      code << "begin"
      code.indent=2
      code << "--default assignments"
      code << "state_v   := state_r;"
      code << "control_v := control_init;"
      code << "done_v    := CFALSE;"
      code << "--transitions"
      code << case_statement(controler)
      code << "state_c   <= state_v;"
      code << "control_c <= control_v;"
      code << "done_c    <= done_v;"
      code.indent=0
      code << "end process;"
      code
    end

    def case_statement controler
      code=Code.new
      code << "case state_v is"
      code.indent=2
      code << gen_idle(controler)
      controler.states.each{|state| code << gen_state(state)}
      code << "when others => null;"
      code.indent=0
      code << "end case;"
      code
    end

    def gen_idle controler
      code=Code.new
      first_state=controler.states.first
      code << "when idle =>"
      code.indent=2
      code << "if start=CTRUE then"
      code.indent=4
      code << "state_v := #{first_state.name};"
      code.indent=2
      code << "end if;"
      code.indent=0
      code
    end

    def gen_state state
      code=Code.new
      code << "when #{state.name} =>"
      code.indent=2
      code << gen_actions(state)
      code << gen_next(state)
      code.indent=0
      code
    end

    def gen_actions state
      code=Code.new
      state.controls.each do |resource_h|
        resource,value=resource_h.first
        case resource
        when HW::Alu,HW::Mux
          value=translate_to_vhdl_op(value) if resource.is_a?(HW::Alu)
          code << "control_v.#{resource.name.downcase} := #{value};"
        when HW::Ram
          code << "control_v.#{resource.name.downcase}_en := RAM_ENABLE;"
          code << "control_v.#{resource.name.downcase}_wr := #{value};"
        end
      end
      code
    end

    def gen_next state
      code=Code.new
      if ctrlIte=state.branching
        code << "if status.#{ctrlIte.status_reg.name}=CTRUE then"
        code.indent=2
        code << "state_v := #{ctrlIte.true_state.name};"
        code.indent=0
        code << "else"
        code.indent=2
        code << "state_v := #{ctrlIte.false_state.name};"
        code.indent=0
        code << "end if;"
      else
        if state.next_states.any?
          state.next_states.each do |next_state|
            code << "state_v := #{next_state.name};"
          end
        else
          code << "done_v := CTRUE;"
          code << "state_v := idle;"
        end
      end
      code
    end

    def interface_connect
      code=Code.new
      code << "-- interface connect"
      code << "control   <= control_c;"
      code << "done      <= done_r;"
      code
    end
    #============== datapath ===============
    def gen_datapath fsmd
      @vhdl_files << filename="#{fsmd.name}_datapath.vhd"
      puts " |-->[+] datapath ".ljust(50,'.')+' '+filename
      code=Code.new
      code << header
      code << ieee_header
      code << fidelio_header
      code << fsmd_header(fsmd)
      code << datapath_entity(fsmd.datapath)
      code << datapath_archi(fsmd.datapath)
      code.save_as filename
    end

    def datapath_entity datapath
      code=Code.new
      code << "entity #{datapath.name}_datapath is"
      code.indent=2
      code << datapath_ports(datapath)
      code.indent=0
      code << "end #{datapath.name}_datapath;"
      code
    end

    def datapath_ports datapath
      code=Code.new
      code << "port("
      code.indent=2
      code << clocking
      code << "control : in  control_type;"
      code << "status  : out status_type;"
      datapath.inputs.each {|port| code << "#{port.name} : in  #{port.type};"}
      datapath.outputs.each{|port| code << "#{port.name} : out #{port.type};"}

      code.indent=0
      code << ");"
      code
    end

    def datapath_archi datapath
      @signals={} #collect signals
      code_consts          =  consts_code(datapath)
      code_mux_regs        =  mux_regs(datapath)
      code_regs            =  regs(datapath)
      code_mux_alus        =  mux_alus(datapath)
      code_mux_rams        =  mux_rams(datapath)
      code_status_bits     =  status_bits_code(datapath)
      code_outputs         =  outputs_code(datapath)
      #----------------------------------
      code=Code.new
      code.newline
      code << "architecture rtl of #{datapath.name}_datapath is"
      code.indent=2
      code << decl_signals(datapath,@signals)
      code.indent=0
      code << "begin"
      code.indent=2
      code << code_consts
      code << code_mux_regs
      code << code_mux_alus
      code << code_mux_rams
      code << code_status_bits
      code << code_outputs
      code.indent=0
      code << "end rtl;"
      #puts code.finalize
      code
    end

    def consts_code datapath
      code=Code.new
      code << "-- -------- constants ---------"
      consts=datapath.components.select{|comp| comp.is_a?(HW::Cst)}
      consts.each do |const|
        output=const.output
        signal=output.signals.first
        val = translate_to_vhdl_value(const.value.val,const.type)
        code << "#{signal.name} <= #{val};"
      end
      code
    end

    def outputs_code datapath
      code=Code.new
      code << "-- -------- outputs ---------"
      datapath.outputs.each do |output|
        source_wire=output.pred_edges.first
        signal=source_wire.infos[:signal]
        code << "#{output.name} <= #{signal.name};"
      end
      code
    end

    def decl_signals datapath,signals_h
      hash={}
      suppress_datapath_io(datapath,signals_h)
      signals_h.each{|name,sig| (hash[sig.type]||=[]) << sig}
      code=Code.new
      txt=""
      hash.each do |type,sigs|
        groups=sigs.map(&:name).sort.each_slice(5).to_a
        groups.each do |group|
          code << "signal #{group.join(',')} : #{type};"
        end
      end
      code
    end

    def suppress_datapath_io datapath,signals_h
      io_names =datapath.inputs.map(&:name).map(&:downcase)
      to_delete=[]
      signals_h.each do |name,sig|
        if io_names.include? name
          to_delete << name
        end
      end
      to_delete.each{|name| signals_h.delete(name)}
    end

    def mux_regs datapath
      code=Code.new
      muxes=datapath.components.select{|comp| comp.is_a?(HW::Mux)}
      muxes=muxes.select{|mux| mux.output.get_sink_components.first.is_a? HW::Reg}
      muxes.each do |mux|
        register_component(mux)
        reg=mux.output.get_sink_components.first
        vars=reg.implementation_of.map{|var| var.name.to_s}.sort.reverse.join(",")
        code << "-- "+"MUX and REG #{reg.name} (mapping of : #{vars})".center(60,'-')
        code << mux_instance(mux)
        code.newline
        code << reg_instance(reg)
      end
      code
    end

    def mux_instance mux
      size=mux.size
      type=mux.output.type
      register_component(mux)
      code=Code.new
      code << "#{mux.name} : entity fidelio_lib.mux_#{size}_#{type}"
      code.indent=2
      code << "port map("
      code.indent=4
      code << "command => control.#{mux.name.downcase},"
      mux.inputs.each do |input|
        edge=input.pred_edges.first
        signal=edge.infos[:signal]
        @signals[signal.name]||=signal
        code << "#{input.name} => #{signal.name},"
      end
      output=mux.output
      signal=output.signals.first
      code << "#{output.name} => #{signal.name}"
      code.indent=2
      code << ");"
      code.indent=0
      code
    end

    def regs datapath
      regs=datapath.components.select{|comp| comp.is_a?(HW::Reg)}
      code=Code.new
      regs.each{|reg| code << reg_instance(reg)}
      code
    end

    def reg_instance reg
      code=Code.new
      register_component(reg)
      code << "#{reg.name} : entity fidelio_lib.reg_#{reg.type}"
      code.indent=2
      code << "port map("
      code.indent=4
      code << clocking_map
      reg.inputs.each do |input|
        edge=input.pred_edges.first
        signal=edge.infos[:signal]
        @signals[signal.name]||=signal
        code << "#{input.name} => #{signal.name},"
      end
      reg.outputs.each do |output|
        edge=output.succ_edges.first
        signal=edge.infos[:signal]
        @signals[signal.name]||=signal
        code << "#{output.name} => #{signal.name},"
      end
      code.indent=2
      code << ");"
      code.indent=0
      code
    end

    def mux_alus datapath
      code=Code.new
      alus=datapath.components.select{|comp| comp.is_a? HW::Alu}
      alus.each do |alu|
        register_component alu
        code << "-- "+"MUXES & ALU #{alu.name}".center(60,'-')
        prev_comps=alu.inputs.collect{|input| input.pred_edges.map{|edge| edge.source.component}}.flatten.uniq
        muxes=prev_comps.select{|comp| comp.is_a? HW::Mux}
        muxes.each{|mux| code << mux_instance(mux)}
        comp_name=alu.name
        code << "#{alu.name} : entity fidelio_lib.#{comp_name}"
        code.indent=2
        code << "port map("
        code.indent=4
        alu.inputs.each do |input|
          signal=input.pred_edges.map{|edge| edge.infos[:signal]}.first
          @signals[signal.name]||=signal
          code << "#{input.name} => #{signal.name},"
        end
        output=alu.output
        signal=output.signals.first
        @signals[signal.name]||=signal
        code << "op => control.#{alu.name},"
        code << "#{output.name} => #{signal.name}"
        code.indent=2
        code << ");"
        code.indent=0
      end
      code
    end

    def mux_rams datapath
      code=Code.new
      rams=datapath.components.select{|comp| comp.is_a? HW::Ram}
      rams.each do |ram|
        register_component ram
        code << "-- "+"MUXES & RAM #{ram.name}".center(60,'-')
        prev_comps=ram.inputs.collect{|input| input.pred_edges.map{|edge| edge.source.component}}.flatten.uniq
        muxes=prev_comps.select{|comp| comp.is_a? HW::Mux}
        muxes.each{|mux| code << mux_instance(mux)}
        comp_name=infer_component_name_from(ram)
        code << "#{ram.name} : entity fidelio_lib.#{comp_name}"
        code.indent=2
        code << "port map("
        code.indent=4
        code << "clk => clk,"
        code << "en  => control.#{ram.name.downcase}_en,"
        code << "wr  => control.#{ram.name.downcase}_wr,"
        ram.inputs.each do |input|
          signal=input.pred_edges.map{|edge| edge.infos[:signal]}.first
          @signals[signal.name]||=signal
          if input.name=="address"
            code << "#{input.name} => unsigned(#{signal.name}),"
          else
            code << "#{input.name} => #{signal.name},"
          end
        end
        output=ram.output
        signal=output.signals.first
        signal_out_name= signal ? signal.name : "open"
        (@signals[signal_out_name]||=signal) if signal
        code << "#{output.name} => #{signal_out_name}"
        code.indent=2
        code << ");"
        code.indent=0
      end
      code
    end

    def status_bits_code datapath
      code=Code.new
      code << "-- status bits"
      regs=datapath.components.select{|comp| comp.is_a?(HW::StatusReg)}
      regs.each do |reg|
        edge=reg.input.pred_edges.first
        signal=edge.infos[:signal]
        code << "status.#{reg.name.downcase} <= #{signal.name}(0);"
      end
      code
    end

    def status_regs_code datapath
      code=Code.new
      code << "-- status regs"
      regs=datapath.components.select{|comp| comp.is_a?(HW::StatusReg)}
      code << "status_bit_regs_p:process(reset_n,clk)"
      code << "begin"
      code.indent=2
      code << "if reset_n='0' then"
      code.indent=4
      regs.each do |reg|
        signal=reg.input.pred_edges.first
        code << "status.#{reg.name.downcase} <= '0';"
      end
      code.indent=2
      code << "elsif rising_edge(clk) then"
      code.indent=4
      regs.each do |reg|
        edge=reg.input.pred_edges.first
        signal=edge.infos[:signal]
        code << "status.#{reg.name.downcase} <= #{signal.name}(0);"
      end
      code.indent=2
      code << "end if;"
      code.indent=0
      code << "end process;"
      code
    end

    #=============== testbench =============
    def gen_tb fsmd
      @vhdl_files << filename="#{fsmd.name}_tb.vhd"
      log " |-->[+] testbench".ljust(50,'.')+' '+filename
      code=Code.new
      code << header
      code << ieee_header
      code << fsmd_header(fsmd)
      code << tb_entity(fsmd)
      code << tb_arch(fsmd)
      code.save_as filename
    end

    def tb_entity fsmd
      code=Code.new
      code << "entity #{fsmd.name}_tb is"
      code << "end entity;"
      code.newline
      code
    end

    def tb_arch fsmd
      code=Code.new
      code << "architecture bhv of #{fsmd.name}_tb is"
      code.indent=2
      code << decl_clk_reset
      code << decl_tb_signals(fsmd)
      code.indent=0
      code << "begin"
      code.indent=2
      code << gen_clk_reset
      code << cycle_count
      code << instance_fsmd(fsmd)
      code << stim_process(fsmd)
      code.indent=0
      code << "end bhv;"
      code.newline
      code
    end

    def decl_clk_reset
      code=Code.new
      code.newline
      code << "constant HALF_PERIOD : time := 5 ns;"
      code.newline
      code << "signal clk     : std_logic := '0';"
      code << "signal reset_n : std_logic := '0';"
      code << "signal sreset  : std_logic := '0';"
      code << "signal running : boolean   := true;"
      code.newline
      code << "procedure wait_cycles(n : natural) is"
      code << " begin"
      code << "   for i in 1 to n loop"
      code << "     wait until rising_edge(clk);"
      code << "   end loop;"
      code << " end procedure;"
      code.newline
      code << "procedure toggle(signal s : inout std_logic) is"
      code << " begin"
      code << "   s <= not(s);"
      code << "   wait until rising_edge(clk);"
      code << " end procedure;"
      code.newline
      code << "procedure gen_pulse(signal s : inout std_logic) is"
      code << " begin"
      code << "   toggle(s);"
      code << "   toggle(s);"
      code << " end procedure;"
      code.newline
      code
    end

    def decl_tb_signals fsmd
      code=Code.new
      code << "signal start       : std_logic := CFALSE;"
      code << "signal done        : std_logic;"
      code << "signal cycle_count : integer;"
      fsmd.inputs.each{|input| code << "signal #{input.name} : #{input.type};"}
      fsmd.outputs.each{|output| code << "signal #{output.name} : #{output.type};"}
      code.newline
      code
    end

    def gen_clk_reset
      code=Code.new
      code.newline
      code << "-- clock generation & asynchronous reset"
      code << "clk <= not(clk) after HALF_PERIOD when running else clk;"
      code.newline
      code << "reset_n <= '0','1' after 66 ns;"
      code.newline
      code
    end

    def instance_fsmd fsmd
      name=fsmd.name
      code=Code.new
      code << "DUT : entity #{name}_lib.#{name}_fsmd(RTL)"
      code.indent=2
      code << "port map("
      code.indent=4
      code << clocking_map
      code << "-- IP control"
      code << "start => start,"
      code << "done  => done,"
      if fsmd.inputs.any? or fsmd.outputs.any?
        code << "-- i/o data"
        fsmd.inputs.each{|input| code << "#{input.name}    => #{input.name},"}
        fsmd.outputs.each{|output| code << "#{output.name} => #{output.name},"}
      end
      code.indent=2
      code << ");"
      code.newline
      code.indent=0
      code
    end

    def stim_process fsmd
      code=Code.new
      code << "stim: process"
      code << "begin"
      code.indent=2
      code << "report \"starting simulation...\";"
      code << init_inputs(fsmd)
      code << "report \"waiting for reset_n...\";"
      code << "wait until reset_n='1';"
      code << "report \"done\";"
      code << "wait_cycles(10);"
      code << "-- actual stimuli"
      code << "gen_pulse(start);"
      code << "wait_cycles(100);"
      code << "report \"waiting for 'done' signal...\";";
      code << "wait until done='1' or cycle_count=1000;"
      code.newline
      code << "wait_cycles(30);"
      code << "running <= false;"
      code << "report \"simulation end\";"
      code << "wait;--forever"
      code.indent=0
      code << "end process;"
      code.newline
      code
    end

    def cycle_count
      code=Code.new
      code << "cycle_count_p: process(reset_n,clk)"
      code << "begin"
      code.indent=2
      code << "if reset_n='0' then"
      code.indent=4
      code << "cycle_count <= 0;"
      code.indent=2
      code << "elsif rising_edge(clk) then"
      code.indent=4
      code << "cycle_count <= cycle_count + 1;"
      code.indent=2
      code << "end if;"
      code.indent=0
      code << "end process;"
    end

    def init_inputs fsmd
      code=Code.new
      fsmd.datapath.inputs.each do |input|
        code << "#{input.name} <= #{default_vhdl_value(input.type)};"
      end
      code
    end

    #=============== helpers ===============
    def header
      code=Code.new
      code << "-"*80
      code << "-- Automatically generated by FIDELIO HLS compiler"
      code << "--"
      code << "-- Fidelio HLS developer : Jean-Christophe Le Lann"
      code << "-- mail : jean-christophe.le_lann@ensta-bretagne.fr"
      code << "-- date   : #{Date.today}"
      code << "-"*80
      code
    end

    def ieee_header
      code=Code.new
      code << "library ieee;"
      code << "use ieee.std_logic_1164.all;"
      code << "use ieee.numeric_std.all;"
      code.newline
      code
    end

    def clocking
      code=Code.new
      code << "reset_n : in  std_logic;"
      code << "clk     : in  std_logic;"
      code
    end

    def clocking_map
      code=Code.new
      code << "reset_n => reset_n,"
      code << "clk     => clk,"
      code
    end

    # =========== GHDL script =============
    def gen_ghdl_script fsmd
      name=fsmd.name
      filename="#{name}_script.x"
      puts " |-->[+] ghdl script".ljust(50,'.')+' '+filename
      code=Code.new
      code << "echo \"=> cleaning...\""
      code << "rm -rf *.o *.cf"
      code << "rm -rf #{name}_tb"
      code << "rm -rf #{name}_tb.ghw"
      @vhdl_files.each do |filename|
        library=filename.match(/^fidelio/) ? "fidelio_lib" : "#{name}_lib"
        code << "echo \"=> analyzing #{filename}\""
        code << "ghdl -a --work=#{library} #{filename}"
      end
      code << "echo \"=> elaborating #{name}_tb\""
      code << "ghdl -e --work=#{name}_lib #{name}_tb"
      code << "ghdl -r #{name}_tb --wave=#{name}_tb.ghw"
      code << "gtkwave #{name}_tb.ghw #{name}_tb.sav"
      code.save_as filename
      ghdl_sav_file(fsmd)
      puts "      |--> given execution rights"
      system("chmod +x #{filename}")
    end

    def ghdl_sav_file fsmd
      #check if it exists
      filename="#{fsmd.name}_tb.sav"
      unless File.exist?(filename)
        sav=Code.new
        sav.save_as filename
      end
    end

    def sanitize
      @vhdl_files.each do |filename|
        vhdl=IO.read(filename)
        vhdl.gsub!(/,(\s*\))/,"\\1")
        vhdl.gsub!(/;(\s*\))/,"\\1")
        IO.write(filename,vhdl)
      end
    end

  end
end
