require_relative 'code'

module VHDL



  class Printer

    def print model
      name=model.name+".vhd"
      code=Code.new
      code << header=print_header(model)
      code.newline
      code << entity=print_entity(model.entity)
      code.newline
      code << archi =print_archi(model.archi)
      code.save_as name
      puts code.finalize
    end

    def print_header model
      code=Code.new
      code << "library ieee;"
      code << "use ieee.std_logic_1164.all;"
      code << "use ieee.numeric_std.all;"
      code
    end

    def print_entity entity
      code=Code.new
      code << "entity #{entity.name} is"
      code.indent=2
      code << "port("
      code.indent=4
      code << "reset_n : in std_logic;"
      code << "clk     : in std_logic;"
      entity.ports.each do |port|
         code << port.code
      end
      code.indent=2
      code << ");"
      code.indent=0
      code << "end #{entity.name};"
      code
    end

    def print_archi archi
      code=Code.new
      code << "architecture #{archi.name} of #{archi.entity.name} is"
      code << "begin"
      code.indent=2
      archi.body.each do |element|
        code << element.code
      end
      code.indent=0
      code.newline
      code << "end #{archi.name};"
      code
    end
  end

  class Input
    def code
      "#{name} : in  #{type};"
    end
  end

  class Output
    def code
      "#{name} : out #{type};"
    end
  end

  class Instance #reopen Struct !
    def code
      code=Code.new
      code.newline
      arch ="(#{arch_name})" if arch_name
      code << "#{self.name} : entity #{lib}.#{entity_name}#{arch}"
      code.indent=2
      code << "port map("
      code.indent=4
      port_map.each do |hash|
        hash.each{|formal,actual| code << "#{formal} => #{actual},"}
      end
      code.indent=2
      code << ");"
      code.indent=0
      code
    end
  end
end
