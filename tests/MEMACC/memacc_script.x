echo "=> cleaning..."
rm -rf *.o *.cf
rm -rf memacc_tb
rm -rf memacc_tb.ghw
echo "=> analyzing fidelio_type_package.vhd"
ghdl -a --work=fidelio_lib fidelio_type_package.vhd
echo "=> analyzing fidelio_reg_int8.vhd"
ghdl -a --work=fidelio_lib fidelio_reg_int8.vhd
echo "=> analyzing fidelio_mux_2_int8.vhd"
ghdl -a --work=fidelio_lib fidelio_mux_2_int8.vhd
echo "=> analyzing fidelio_alu_int8_int8_int8.vhd"
ghdl -a --work=fidelio_lib fidelio_alu_int8_int8_int8.vhd
echo "=> analyzing memacc_type_package.vhd"
ghdl -a --work=memacc_lib memacc_type_package.vhd
echo "=> analyzing memacc_controler.vhd"
ghdl -a --work=memacc_lib memacc_controler.vhd
echo "=> analyzing memacc_datapath.vhd"
ghdl -a --work=memacc_lib memacc_datapath.vhd
echo "=> analyzing memacc_fsmd.vhd"
ghdl -a --work=memacc_lib memacc_fsmd.vhd
echo "=> analyzing memacc_tb.vhd"
ghdl -a --work=memacc_lib memacc_tb.vhd
echo "=> elaborating memacc_tb"
ghdl -e --work=memacc_lib memacc_tb
ghdl -r memacc_tb --wave=memacc_tb.ghw
gtkwave memacc_tb.ghw memacc_tb.sav
