puts "cleaning directory..."
to_delete=[]
to_delete << Dir["*.vhd"]
to_delete << Dir["*.x"]
to_delete << Dir["*.dot"]
to_delete << Dir["*.o"]
to_delete << Dir["*.cf"]
to_delete.flatten!

to_delete.each{|filename|
  puts "- delete #{filename}"
  File.delete(filename)
}
puts "-"*60
puts "#files deleted : #{to_delete.size}"
puts "-"*60
